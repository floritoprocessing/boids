int nrOfBirds=350;

Bird[] bird=new Bird[nrOfBirds+1];

void setup() {
  size(600,600,P3D);
  createBirds();
}

void draw() {
  background(128,192,255);
  ellipseMode(DIAMETER);
  for(int i=1;i<=nrOfBirds;i++) {bird[i].updateVector();}
  for(int i=1;i<=nrOfBirds;i++) {bird[i].updatePosition();}
  for(int i=1;i<=nrOfBirds;i++) {bird[i].updateTiredness();}
  for(int i=1;i<=nrOfBirds;i++) {bird[i].drawBird();}
}

void createBirds() {
  for(int i=1;i<=nrOfBirds;i++) {
    bird[i]=new Bird(i);
    bird[i].init();
  }
}

void keyPressed() {
  createBirds();
}

class Bird {
  int nr;
  float x,y;
  float xmov,ymov, tmpxmov, tmpymov, newXmov, newYmov;
  
  float closeDistance=30;
  float influence=0.0;
  
  float borderRange=50;
  float leftBorder=borderRange, rightBorder=width-borderRange;
  float topBorder=borderRange, bottomBorder=height-borderRange;
  float turnDeg=0.0, minTurnDeg=0.01*PI, maxTurnDeg=0.06*PI;
  boolean turn=false;
  
  float maxSpeed=random(3,5);
  float curveDeg=0;
  
  boolean tired;
  float tiredSpeed=0.01;
  float tiredDistance;
  float relaxDistance;
  float totalDistance;
  
  Bird(int nr) {this.nr=nr;}
  
  void init() {
    x=random(width);y=random(height);
    xmov=random(-3,3);ymov=random(-3,3);
    influence=random(0.03,0.04);
    tiredDistance=random(2000,7000);
    relaxDistance=random(2,8);
    totalDistance=0;
    tired=true;totalDistance=0;
  }
  
  void updateVector() {
    newXmov=xmov; newYmov=ymov;
    
    // fly random curves:
    // ------------------
    curveDeg=(2*curveDeg+TWO_PI*random(-0.5,0.5)/15.0)/3.0;
    float Si=sin(curveDeg); float Co=cos(curveDeg); 
    tmpxmov=newXmov*Co-newYmov*Si; tmpymov=newYmov*Co+newXmov*Si;
    newXmov=tmpxmov; newYmov=tmpymov;
    
    // adjust flightpath to close birds:
    // ---------------------------------
    for (int i=1;i<=nrOfBirds;i++) {
      if (i!=nr) {
        if (dist(x+newXmov,y+newYmov,bird[i].x,bird[i].y)<closeDistance) {
          newXmov+=influence*bird[i].xmov;
          newYmov+=influence*bird[i].ymov;
        }
      }
    }
    
    // adjust flightpath if close to border:
    // -------------------------------------
    if (turn==false&&x<leftBorder&&newXmov<0) {
      turn=true;turnDeg=random(minTurnDeg,maxTurnDeg);if (newYmov>0) {turnDeg*=-1;}
    }
    if (turn==false&&x>rightBorder&&newXmov>0) {
      turn=true;turnDeg=random(minTurnDeg,maxTurnDeg);if (newYmov<0) {turnDeg*=-1;}
    }
    if (turn==false&&y<topBorder&newYmov<0) {
      turn=true;turnDeg=random(minTurnDeg,maxTurnDeg);if (newXmov<0) {turnDeg*=-1;}
    }
    if (turn==false&&y>bottomBorder&newYmov>0) {
      turn=true;turnDeg=random(minTurnDeg,maxTurnDeg);if (newXmov>0) {turnDeg*=-1;}
    }
    if (turn) {
      if (random(1)<0.04) {turn=false;}
      turnDeg+=random(-minTurnDeg/2.0,minTurnDeg/2.0);
      Si=sin(turnDeg); Co=cos(turnDeg);
      tmpxmov=newXmov*Co-newYmov*Si; tmpymov=newYmov*Co+newXmov*Si;
      float speedUp=random(1.0,1.1);
      newXmov=speedUp*tmpxmov; newYmov=speedUp*tmpymov;
    }
        
    // adjust maximum speed:
    // ---------------------
    float speed=dist(0,0,newXmov,newYmov);
    if (speed>maxSpeed) {
      newXmov*=(maxSpeed/speed);
      newYmov*=(maxSpeed/speed);
    }
  }
  
  void updatePosition() {
    xmov=newXmov; ymov=newYmov;
    if (tired==false) {
      x+=xmov; y+=ymov; 
    } else {
      x+=xmov*tiredSpeed; y+=ymov*tiredSpeed;
    }
    while (x>width) {x-=width;}
    while (x<1) {x+=width;}
    while (y>height) {y-=height;}
    while (y<1) {y+=height;}
  }
  
  void updateTiredness() {
    if (tired==false) {
      totalDistance+=dist(0,0,xmov,ymov);
      if (totalDistance>tiredDistance) {tired=true;totalDistance=0;}
    } else {
      totalDistance+=dist(0,0,xmov,ymov)*tiredSpeed;
      if (totalDistance>relaxDistance) {tired=false;totalDistance=0;}
    }
    if (tired && dist(x,y,mouseX,mouseY)<40) {tired=false;totalDistance=0;}
  }
  
  void drawBird() {
    if (!tired) {
      noFill();ellipse(x,y,4,4);
    } else {
      fill(0);ellipse(x,y,3,3);
    }
  }
}
