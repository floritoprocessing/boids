package boidsv04;

import java.util.Date;


public class BoidsWorld {
	
	public boolean printErrorMessages = true;
	
	/*
	 * World parameters
	 */
	public final short maxBoids;
	public final float speed;
	
	//public boolean constrainLatitude = true;
	private float maxLatitude = (float)(75 * Math.PI/180.0);
	//private float maxLatitudeYoverXZ = Math.abs((float)Math.tan(maxLatitude));
	
	//public boolean calculateRotationForOrientation = true;
		
	public float maxForce;
	
	public final int width;
	public final int height;
	public final int depth;
	public final int perceptionDistance;
	public final float maximumPerceptionDistance;
	public final float invPerceptionDistance;
	public final int gridWidth;
	public final int gridHeight;
	public final int gridDepth;
	
	/**
	 * The space registry is a flat 4-dimensional array of 
	 * <ul>
	 * <li> z (0..gridDepth) by
	 * <li> y (0..gridHeight) by
	 * <li> x (0..gridWidth) by
	 * <li> ids (0..maxBoids)
	 * </ul>
	 * <p><b>Note: Do not modify this array outside this class</b>
	 * <p>As an example, to get all boids at position (3,1,4) consider this code:
	 * <p> 
	 * <code><pre>
	 * int indexX = 3, indexY = 1, indexZ = 4;
	 * int indexStartBoids = (indexX*maxBoids + indexY*maxBoids*gridWidth + indexZ*maxBoids*gridWidth*gridHeight);
	 * // or indexStartBoids = ((indexZ*gridHeight + indexY)*gridWidth + indexX) * maxBoids;
	 * int indexEndBoids = indexStartBoids + maxBoids;
	 * for (int i=indexStartBoids;i&lt;indexEndBoids;i++) {
	 *   //do stuff here
	 *   println(spaceRegistry[i]);
	 * }
	 * </pre></code>
	 * 
	 */
	public final short[] spaceRegistry;
	private final short[] emptySpaceRegistry;
	
	/**
	 * for pointing at the sub index, where to 'place' the next bird<br>
	 * used only in {@link #updateSpaceGridPositions()}
	 */
	private final int[] spaceRegistryPointerToNext;
	private final int[] emptySpaceRegistryPointerToNext;
	
	/**
	 * This array contains all the indexes of the neighbouring cells per cell of the spaceRegistry.
	 * The array points at the first boid (which might be -1) of the neighbouring cells
	 * This array is in size: gridWidth*gridHeight*gridDepth*27 (since there are 9+9+9 neighbour cells including itself)
	 * <br>
	 * To get the correct neighbour indexes:
	 * <code><pre>
	 * int startIndex = gridIndexAbsolute[boidId]*27;
	 * int endIndex = startIndex+27;
	 * neighbourIndexes[startIndex] ... &lt;neighbourIndexes[endIndex] 
	 * </pre></code>
	 */
	private final int[] neighbourIndexes;
	
	/**
	 * array pointing at all the neighbour ids per boid maxBoids*maxBoids.
	 * Gets set by {@link #registerNeighbours()}
	 */
	private short[] neighbourIdRegistry = new short[0];
	private short[] emptyNeighbourIdRegistry = new short[0];
	
	/*
	 * For retrieving boid neighbours
	 */
	private short[] neighboursPerBoid;
	private short[] emptyNeighboursPerBoid;
	
	
	//private float[] distanceRegistry;
	//private int distanceRegistrySize;
	
	
	
	
	
	
	/*
	 * Boid parameters
	 * !watch out! for each parameter, change the increaseBoids method
	 */
	private short boidAmount = 0;
	private short[] id = new short[0];
	/**
	 * x-position in grid
	 * <p><b>Note: Do not modify this array outside this class</b>
	 */
	public int[] gridIndexX = new int[0];
	/**
	 * y-position in grid
	 * <p><b>Note: Do not modify this array outside this class</b>
	 */
	public int[] gridIndexY = new int[0];
	/**
	 * z-position in grid
	 * <p><b>Note: Do not modify this array outside this class</b>
	 */
	public int[] gridIndexZ = new int[0];
	/**
	 * Just a shortcut to (gridIndexZ[i]*gridHeight + gridIndexY[i])*gridWidth + gridIndexX[i].
	 * To get to the spaceRegistry index, multiply by {@link #maxBoids}
	 * <p><b>Note: Do not modify this array outside this class</b>
	 */
	public int[] gridIndexAbsolute = new int[0];
	/**
	 * x-position
	 * <p><b>Note: </b> If you want to modify this array, please call {@link #updateInternalRegistry()} after doing so.
	 * <p><b>Note: Do not modify the array length!</b>
	 */
	public float[] x = new float[0];
	/**
	 * y-position
	 * <p><b>Note: </b> If you want to modify this array, please call {@link #updateInternalRegistry()} after doing so.
	 * <p><b>Note: Do not modify the array length!</b>
	 */
	public float[] y = new float[0];
	/**
	 * z-position
	 * <p><b>Note: </b> If you want to modify this array, please call {@link #updateInternalRegistry()} after doing so.
	 * <p><b>Note: Do not modify the array length!</b>
	 */
	public float[] z = new float[0];
	/**
	 * x-movement
	 * <p><b>Note:</b> When updating the array through {@link #moveBoids(boolean)}, the method {@link #constrainSpeed()} is called, constraining the movement vector.
	 * If modifying yourself, you might want to call the method as well.
	 * <p><b>Note: Do not modify the array length!</b>
	 */
	public float[] xm = new float[0];
	/**
	 * y-movement
	 * <p><b>Note:</b> When updating the array through {@link #moveBoids(boolean)}, the method {@link #constrainSpeed()} is called, constraining the movement vector.
	 * If modifying yourself, you might want to call the method as well.
	 * <p><b>Note: Do not modify the array length!</b>
	 */
	public float[] ym = new float[0];
	/**
	 * z-movement
	 * <p><b>Note:</b> When updating the array through {@link #moveBoids(boolean)}, the method {@link #constrainSpeed()} is called, constraining the movement vector.
	 * If modifying yourself, you might want to call the method as well.
	 * <p><b>Note: Do not modify the array length!</b>
	 */
	public float[] zm = new float[0];
	
	public float[] lastX = new float[0];
	public float[] lastY = new float[0];
	public float[] lastZ = new float[0];
	
	/**
	 * Color of each boid
	 * <p><b>Note: Do not modify the array length!</b>
	 */
	public int[] color = new int[0];
	//public ArrayList<Short>[] neighbourIds = new ArrayList<Short>[0];
	
	private float[] wanderLon = new float[0];
	private float[] wanderLat = new float[0];
	
	
	/*public float[] rotationX = new float[0];
	public float[] rotationY = new float[0];*/
	
	public float wanderSphereRadius = 1.0f;// s*speed
	public float wanderRadiansMax = 0.2f;
	
	public float wander = 1.0f;
	public float alignment = 1.0f;
	public float cohesion = 1.0f;
	public float separation = 1.0f;
	/**
	 * The strength of how much the latitude of the bird is contstrained.
	 * @see #setMaxLatitude(float)
	 */
	public float latitude = 1.0f;
	
	public float edgeAvoidance = 1.0f;
	public float edgeAvoidanceFac = 25.0f;
	
	public BoidsWorld(int width, int height, int depth, int perceptionDistance, short maxBoids, float speed) {
		this.maxBoids = maxBoids;
		this.speed = speed;
		this.width=width;
		this.height=height;
		this.depth=depth;
		this.perceptionDistance = perceptionDistance;
		
		float fw = (float)width;
		float fh = (float)height;
		float fd = (float)depth;
		float fp = (float)perceptionDistance;
		
		
		float d = 2*perceptionDistance;
		this.maximumPerceptionDistance = (float)Math.sqrt(d*d+d*d+d*d);
		invPerceptionDistance = 1.0f/perceptionDistance;
		gridWidth = (int)(width/perceptionDistance);
		gridHeight = (int)(height/perceptionDistance);
		gridDepth = (int)(depth/perceptionDistance);
		if ((float)gridWidth!=(fw/fp)) {
			throw new RuntimeException("width ("+width+") must be a multiple of perceptionDistance ("+perceptionDistance+")");
		}
		if ((float)gridHeight!=(fh/fp)) {
			throw new RuntimeException("height ("+height+") must be a multiple of perceptionDistance ("+perceptionDistance+")");
		}
		if ((float)gridDepth!=(fd/fp)) {
			throw new RuntimeException("depth ("+depth+") must be a multiple of perceptionDistance ("+perceptionDistance+")");
		}
		
		wanderSphereRadius = speed;
		maxForce = speed / 7.5f;
		
		spaceRegistry = new short[maxBoids*gridWidth*gridHeight*gridDepth];
		emptySpaceRegistry = new short[maxBoids*gridWidth*gridHeight*gridDepth];
		System.out.println("Space registry of "+gridWidth+"x"+gridHeight+"x"+gridDepth+" and "+maxBoids+" maximum boids per grid. Size: "+emptySpaceRegistry.length);
		for (int i=0;i<emptySpaceRegistry.length;i++) {
			emptySpaceRegistry[i] = (short)-1;
		}
		
		spaceRegistryPointerToNext = new int[gridWidth*gridHeight*gridDepth];
		emptySpaceRegistryPointerToNext = new int[gridWidth*gridHeight*gridDepth];
		int i=0;
		int pointer = 0;
		for (int z=0;z<gridDepth;z++) {
			for (int y=0;y<gridHeight;y++) {
				for (int x=0;x<gridWidth;x++) {
					emptySpaceRegistryPointerToNext[i] = pointer;
					pointer += maxBoids; // of is het d*h*w?
					i++;
				}
			}
		}
	
		/*
		 * Create neighbourIndexes array
		 */
		neighbourIndexes = new int[gridWidth*gridHeight*gridDepth*27];
		System.out.println("Neighbour indexes array of "+gridWidth+"x"+gridHeight+"x"+gridDepth+" and 27 neighbours per cell. Size: "+neighbourIndexes.length);
		int x, y, z;
		int xo, yo, zo;
		int nx, ny, nz; // neighbours
		i=0;
		for (z=0;z<gridDepth;z++) {
			for (y=0;y<gridHeight;y++) {
				for (x=0;x<gridWidth;x++) {
					
					//for each cell get all neighbour indexes of the spaceRegistry
					int skippedBecauseOfEdge = 0;
					for (zo=-1;zo<=1;zo++) for (yo=-1;yo<=1;yo++) for (xo=-1;xo<=1;xo++) {
						nx = x+xo;
						ny = y+yo;
						nz = z+zo;
						// look over the edge
						/*if (nx<0) {
							nx+=gridWidth;
						} else if (nx>=gridWidth) {
							nx-=gridWidth;
						}
						if (ny<0) {
							ny+=gridHeight;
						} else if (ny>=gridHeight) {
							ny-=gridHeight;
						}
						if (nz<0) {
							nz+=gridDepth;
						} else if (nz>=gridDepth) {
							nz-=gridDepth;
						}*/
						// do not look over the edge
						if (nx<0||nx>=gridWidth||ny<0||ny>=gridHeight||nz<0||nz>=gridDepth) {
							skippedBecauseOfEdge++;
						} else {
							neighbourIndexes[i] = ((nz*gridHeight + ny)*gridWidth + nx) * maxBoids;
							i++;
						}
					}
					i += skippedBecauseOfEdge;
					
				}
			}
		}
		System.out.print("i.e. the neighbourIndexes of cell 3/4/5 ");
		int absoluteCellIndex = (5*gridHeight + 4)*gridWidth + 3;
		System.out.println("(absolute index "+absoluteCellIndex+"):");
		int cellIndex = absoluteCellIndex*27;
		for (int n=0;n<27;n++) {
			System.out.print(neighbourIndexes[cellIndex++]+", ");
		}
		System.out.println();
		
		neighbourIdRegistry = new short[maxBoids*maxBoids];
		emptyNeighbourIdRegistry = new short[maxBoids*maxBoids];
		for (i=0;i<neighbourIdRegistry.length;i++) {
			neighbourIdRegistry[i]=-1;
			emptyNeighbourIdRegistry[i]=-1;
		}
		
		neighboursPerBoid = new short[maxBoids];
		emptyNeighboursPerBoid = new short[maxBoids];
		for (i=0;i<maxBoids;i++) {
			neighboursPerBoid[i] = -1;
			emptyNeighboursPerBoid[i] = -1;
		}
		
		//distanceRegistry = new float[maxBoids*maxBoids];
		//distanceRegistrySize = 0;
	}
	
	
	
	
	public void setMaxLatitude(float maxLatitude) {
		this.maxLatitude = maxLatitude;
		//maxLatitudeYoverXZ = (float)Math.tan(maxLatitude);
	}
	
	
	
	
	public float getPerceptionDistance() {
		return perceptionDistance;
	}
	
	public void moveBoids(boolean moveThem) {
		//int amount = x.length;
		if (moveThem) {

			System.arraycopy(x, 0, lastX, 0, boidAmount);
			System.arraycopy(y, 0, lastY, 0, boidAmount);
			System.arraycopy(z, 0, lastZ, 0, boidAmount);
			
			for (int i=0;i<boidAmount;i++) {
				x[i] += xm[i];
				y[i] += ym[i];
				z[i] += zm[i];
			}
			updateInternalRegistry();
			totalBoidBehavior();
		}
		
		
		
		
		
	}

	/**
	 * Updates the internal registry, calling:
	 * <ol>
	 * <li> {@link #constrainInWorld()}
	 * <li> {@link #updateGridPositions()}
	 * <li> {@link #updateSpaceGridPositions()}
	 * <li> {@link #registerNeighbours()}
	 * </ol>
	 */
	private void updateInternalRegistry() {
		constrainInWorld();
		updateGridPositions();
		updateSpaceGridPositions();
		registerNeighbours();
		
	}
	
	
	private void constrainInWorld() {
		//int amount = x.length;
		for (int i=0;i<boidAmount;i++) {
			if (Float.isNaN(x[i]) || Float.isInfinite(x[i])) {
				x[i]=0;
				printErrorMessage("Error (x) with boid "+i+", xyz:"+xm[i]+", "+ym[i]+", "+zm[i]);
			}
			if (Float.isNaN(y[i]) || Float.isInfinite(y[i])) {
				y[i]=0;
				printErrorMessage("Error (y) with boid "+i+", xyz:"+xm[i]+", "+ym[i]+", "+zm[i]);
			}
			if (Float.isNaN(z[i]) || Float.isInfinite(z[i])) {
				z[i]=0;
				printErrorMessage("Error (z) with boid "+i+", xyz:"+xm[i]+", "+ym[i]+", "+zm[i]);
			}
			while (x[i]<0) {
				x[i] += width;
			}
			while (x[i]>=width) {
				x[i] -= width;
			}
			while (y[i]<0) {
				y[i] += height;
			}
			while (y[i]>=height) {
				y[i] -= height;
			}
			while (z[i]<0) {
				z[i] += depth;
			}
			while (z[i]>=depth) {
				z[i] -= depth;
			}
		}
	}

	private void printErrorMessage(String firstMessage) {
		if (printErrorMessages) {
		System.err.println(firstMessage);
		StackTraceElement[] element = (new Error()).getStackTrace();
		/*for (StackTraceElement e:element) {
			System.err.println("\t"+e);
		}*/
		;
		System.err.println((new Date()).toString()+" [...] "+element[1]+" [...]");
		}
	}
	
	private void updateGridPositions() {
		//int amount = x.length;
		for (short i=0;i<boidAmount;i++) {
			gridIndexX[i] = (int)(x[i]*invPerceptionDistance);
			gridIndexY[i] = (int)(y[i]*invPerceptionDistance);
			gridIndexZ[i] = (int)(z[i]*invPerceptionDistance);
			gridIndexAbsolute[i] = (gridIndexZ[i]*gridHeight + gridIndexY[i])*gridWidth + gridIndexX[i];
		}
	}
	
	private void updateSpaceGridPositions() {
		// clearSpaceRegistry();
		System.arraycopy(emptySpaceRegistry, 0, spaceRegistry, 0, spaceRegistry.length);
		
		// clear pointers to first
		System.arraycopy(emptySpaceRegistryPointerToNext, 0, spaceRegistryPointerToNext, 0, spaceRegistryPointerToNext.length);
		
		int xyzIndexToPointer;
		int pointerToSpaceRegistryPlace;
		for (short i=0;i<boidAmount;i++) {
			// use the grid position to point at a 'cell' (xyzIndexToPointer)
			// this 'cell' in the spaceRegistryPointerToNext array points at a place in the space registry, where to place the next bird
			//xyzIndexToPointer = (gridIndexZ[i]*gridHeight + gridIndexY[i])*gridWidth + gridIndexX[i];
			xyzIndexToPointer = gridIndexAbsolute[i];
			pointerToSpaceRegistryPlace = spaceRegistryPointerToNext[xyzIndexToPointer];
			spaceRegistry[pointerToSpaceRegistryPlace] = id[i];
			spaceRegistryPointerToNext[xyzIndexToPointer]++; // move pointer one up for possible next bird in this spaceRegistry cell
		}
	}
	
	
	
	/**
	 * Registers all neighbours EXCEPT itself
	 */
	private void registerNeighbours() {
		
		System.arraycopy(emptyNeighbourIdRegistry, 0, neighbourIdRegistry, 0, neighbourIdRegistry.length);
		int sourceBoidIndex=0;
		
		int absoluteGridIndex;
		
		int neighbourBoidIndex, neighbourBoidIndexEnd;
		boolean moreBoids;
		
		//go through each boid
		for (short sourceBoidID=0;sourceBoidID<boidAmount;sourceBoidID++) {
			
			//boidIds.clear();
			sourceBoidIndex = sourceBoidID*maxBoids;
			
			// get the grid position and corresponding cell
			absoluteGridIndex = gridIndexAbsolute[sourceBoidID]*27;//
			
			// go through all neighbouring cells indexes
			int neighbourCount=0;
			for (int neighbour=0;neighbour<27;neighbour++) {
				
				// index of neighbour in spaceGrid
				neighbourBoidIndex = neighbourIndexes[absoluteGridIndex+neighbour];
				neighbourBoidIndexEnd = neighbourBoidIndex + maxBoids;
				moreBoids = true;
				
				// go through all boids per neighbour:
				while (neighbourBoidIndex<neighbourBoidIndexEnd && moreBoids) {
					short foundBoidID = spaceRegistry[neighbourBoidIndex];
					if (foundBoidID==-1) {
						moreBoids = false;
					} else if (foundBoidID!=sourceBoidID) {
						// add neighbours except when it is itself
						neighbourIdRegistry[sourceBoidIndex+neighbourCount] = foundBoidID;
						neighbourCount++;
					}
					neighbourBoidIndex++;
				}
				
			}
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	private void totalBoidBehavior() {
		
		float posX, posY, posZ;
		float movX, movY, movZ;
		
		float dx, dy, dz;
		double dist, distSquared;
		float px, py, pz;
		float mx, my, mz;
		
		double length;
		double fac;
		short[] neighbourIDs;
		
		float SIN, COS;
		float wanderOffX, wanderOffY, wanderOffZ;
		
		float futureX, futureY, futureZ;
		float yOverX, yOverZ;
		
		float wanderVecX=0, wanderVecY=0, wanderVecZ=0;
		float cohesionVecX=0, cohesionVecY=0, cohesionVecZ=0;
		float alignVecX=0, alignVecY=0, alignVecZ=0;
		float separationVecX=0, separationVecY=0, separationVecZ=0;
		float latitudeVecX=0, latitudeVecY=0, latitudeVecZ=0;
		float edgeVecX=0, edgeVecY=0, edgeVecZ=0;
		
		float steerX, steerY, steerZ;
				
		for (short boidId=0;boidId<boidAmount;boidId++) {
			
			// set pos and mov
			posX = x[boidId];
			posY = y[boidId];
			posZ = z[boidId];
			movX = xm[boidId];
			movY = ym[boidId];
			movZ = zm[boidId];
			
			/*
			 * ---------------------------------------------------------
			 * Wander
			 * static sphereRadius = wanderMax = 2*BIRD_MAX_VELOCITY = 2*15
			 * static radiansMax = 0.2
			 * - offVec
			 * - lat
			 * - lon
			 * 
			 * off = movement //create a copy of movement
			 * lat += random(-radiansMax,radiansMax)
			 * lon += random(-radiansMax,radiansMax)
			 * off.rotX(lat)
			 * off.rotY(lon)
			 * off.setLength(sphereRadius)
			 * 
			 * wander = movement + off
			 * ---------------------------------------------------------
			 */
			
			// wander:
			wanderOffX = movX;
			wanderOffY = movY;
			wanderOffZ = movZ;
			
			// rotate wanderOff around X latitude
			wanderLat[boidId] += (wanderRadiansMax*(Math.random()*2-1));
			SIN = (float)Math.sin(wanderLat[boidId]);
			COS = (float)Math.cos(wanderLat[boidId]);
			my = wanderOffY*COS - wanderOffZ*SIN;
			mz = wanderOffZ*COS + wanderOffY*SIN;
			wanderOffY = my;
			wanderOffZ = mz;
			
			// rotate wanderOff around Y longitude
			wanderLon[boidId] += (wanderRadiansMax*(Math.random()*2-1));
			SIN = (float)Math.sin(wanderLon[boidId]);
			COS = (float)Math.cos(wanderLon[boidId]);
			mx = wanderOffX*COS - wanderOffZ*SIN;
			mz = wanderOffZ*COS + wanderOffX*SIN;
			wanderOffX = mx;
			wanderOffZ = mz;
			
			// normalize to sphere radius (wander max)
			length = Math.sqrt(wanderOffX*wanderOffX + wanderOffY*wanderOffY + wanderOffZ*wanderOffZ);
			if (length!=0) {
				fac = wanderSphereRadius / length;
				wanderOffX *= fac;
				wanderOffY *= fac;
				wanderOffZ *= fac;
			}
			
			wanderVecX = movX + wanderOffX;
			wanderVecY = movY + wanderOffY;
			wanderVecZ = movZ + wanderOffZ;
			
			/* 
			 * ---------------------------------------------------------
			 * Separation
			 * //For each nearby character, 
			 * // a repulsive force is computed by subtracting the positions of our character and the nearby character,  
			 * // normalising, and then applying a 1/r weighting. (works ok)
			 * 
			 * separation = 0;
			 * for each neighbour:
			 *   awayVec = position - neighbour.position
			 *   r = awwayVec.magnitude() // distance to other bird
			 *   awayVec /= r; // normalise
			 *   awayVec /= r; // weighting
			 *   separation += awayVec
			 *   
			 * separation
			 * ---------------------------------------------------------
			 */
			/* 
			 * ---------------------------------------------------------
			 * Cohesion
			 * 
			 * averagePos = 0;
			 * for each neighbour:
			 *   averagePos += neighbour.position
			 * averagePos /= neighbourCount
			 * cohesion = averagePos-position
			 * 
			 * cohesion
			 * ---------------------------------------------------------
			 */ 
			/* 
			 * ---------------------------------------------------------
			 * Align
			 * 
			 * averageMov = 0;
			 * for each neighbour:
			 *   averageMov += neighbour.movement
			 * averageMov /= neighbourCount
			 * align = averageMovement-movement
			 * 
			 * align
			 * ---------------------------------------------------------
			 */
			
			neighbourIDs = getNeighbourIDsOfBoid(boidId);
			
			int neighbourCount=0;
			short neighbourID;
			
			dx = dy = dz = 0; // distance for separation
			separationVecX = separationVecY = separationVecZ = 0;
			px = py = pz = 0; // position for cohesion
			mx = my = mz = 0; // movement for alignment
			
			while (neighbourCount<neighbourIDs.length && (neighbourID = neighbourIDs[neighbourCount])!=-1) {
				// awayVec = position - neighbour.position (separation) 
				dx = posX - x[neighbourID];
				dy = posY - y[neighbourID];
				dz = posZ - z[neighbourID];
				// r = awwayVec.magnitude() // distance to other bird (separation)
				// dist = Math.sqrt(dx*dx+dy*dy+dz*dz);
				// awayVec /= r; // normalise
				// awayVec /= r; // weighting
				// same: awayVec /= (r*r);
				// same: awayVec /= (dist*dst);
				// same: awawVec /= dx*dx+dy*dy+dz*dz
				distSquared = dx*dx+dy*dy+dz*dz;
				fac = 1/distSquared;
				dx *= fac;
				dy *= fac;
				dz *= fac;
				separationVecX += dx;
				separationVecY += dy;
				separationVecZ += dz;
				// to calculate average position (cohesion)
				px += x[neighbourID];
				py += y[neighbourID];
				pz += z[neighbourID];
				// to calculate average movement (alignment)
				mx += xm[neighbourID];
				my += ym[neighbourID];
				mz += zm[neighbourID];
				neighbourCount++;
			}
			
			if (neighbourCount>0) {
				
				fac = 1.0f/neighbourCount;
				
				// to calculate average position (cohesion)
				px *= fac;
				py *= fac;
				pz *= fac;
				cohesionVecX = px-posX;
				cohesionVecY = py-posY;
				cohesionVecZ = pz-posZ;
				
				// to calculate average movement (alignment)
				mx *= fac;
				my *= fac;
				mz *= fac;
				alignVecX = mx-movX;
				alignVecY = my-movY;
				alignVecZ = mz-movZ;
				
			} else {
				
				cohesionVecX = 0;
				cohesionVecY = 0;
				cohesionVecZ = 0;
				alignVecX = 0;
				alignVecY = 0;
				alignVecZ = 0;
				
			}
			
			/*
			 * ---------------------------------------------------------
			 * Constrain latitude
			 * ---------------------------------------------------------
			 */
			
			latitudeVecX = movX;
			latitudeVecZ = movZ;
			
			// Pitch: tan(P) = sqrt(x^2 + y^2)/ z
			// http://stackoverflow.com/questions/2782647/how-to-get-yaw-pitch-and-roll-from-a-3d-vector
			float tanPitch = (float) Math.sqrt(movX * movX + movZ * movZ) / movY;
			float pitch = (float)Math.atan(tanPitch);
			
			if (pitch>maxLatitude) {
				latitudeVecY = -speed;
			} else if (pitch<-maxLatitude) {
				latitudeVecY = speed;
			} else {
				latitudeVecY = 0;
			}
			
			/*
			 * ---------------------------------------------------------
			 * Edge avoidance
			 * ---------------------------------------------------------
			 */
			
			// reset vector
			edgeVecX = edgeVecY = edgeVecZ = 0;
			
			// create future position
			futureX = posX + edgeAvoidanceFac*movX;
			futureY = posY + edgeAvoidanceFac*movY;
			futureZ = posZ + edgeAvoidanceFac*movZ;
			
			if (futureX>width) {
				edgeVecX = width - futureX;
			} else if (futureX<0) {
				edgeVecX = -futureX;
			}
			if (futureY>height) {
				edgeVecY = height - futureY;
			} else if (futureY<0) {
				edgeVecY = -futureY;
			}
			if (futureZ>depth) {
				edgeVecZ = depth - futureZ;
			} else if (futureZ<0) {
				edgeVecZ = -futureZ;
			}
			
			
			/* 
			 * ---------------------------------------------------------
			 * TotalBird steering:
			 * ** maxForce = 2
			 * ** maxVel = 15
			 * steering = wander.norm() * wanderFac 
			 * 				+ separation.norm() * seperationFac
			 * 				+ cohesion.norm() * cohesionFac
			 * 				+ align.norm() * alignFac
			 * 				+ latitudeVec * latitudeFac
			 * 
			 * apply steering:
			 * 
			 * steerForce = steering.normalizedTo(maxForce)
			 * velocity = velocity + steerForce
			 * velocity.normalizeTo(maxVel)
			 * --------------------------------------------------------- 
			 */
			
			// normalize wander to wanderFac
			length = Math.sqrt(wanderVecX*wanderVecX + wanderVecY*wanderVecY + wanderVecZ*wanderVecZ);
			if (length!=0) {
				fac = wander / length;
				wanderVecX *= fac; wanderVecY *= fac; wanderVecZ *= fac;
			}
			// normalize separation
			length = Math.sqrt(separationVecX*separationVecX + separationVecY*separationVecY + separationVecZ*separationVecZ);
			if (length!=0) {
				fac = separation / length;
				separationVecX *= fac; separationVecY *= fac; separationVecZ *= fac;
			}
			// normalize cohesion
			length = Math.sqrt(cohesionVecX*cohesionVecX + cohesionVecY*cohesionVecY + cohesionVecZ*cohesionVecZ);
			if (length!=0) {
				fac = cohesion / length;
				cohesionVecX *= fac; cohesionVecY *= fac; cohesionVecZ *= fac;
			}
			// normalize align
			length = Math.sqrt(alignVecX*alignVecX + alignVecY*alignVecY + alignVecZ*alignVecZ);
			if (length!=0) {
				fac = alignment / length;
				alignVecX *= fac; alignVecY *= fac; alignVecZ *= fac;
			}
			// normalize latitudeVec. 
			length = Math.sqrt(latitudeVecX*latitudeVecX + latitudeVecY*latitudeVecY + latitudeVecZ*latitudeVecZ);
			if (length!=0) {
				fac = latitude / length;
				latitudeVecX *= fac; latitudeVecY *= fac; latitudeVecZ *= fac;
			}
			// normalize edgeVec. 
			/*length = Math.sqrt(edgeVecX*edgeVecX + edgeVecY*edgeVecY + edgeVecZ*edgeVecZ);
			if (length!=0) {
				fac = edgeAvoidance / length;
				edgeVecX *= fac; edgeVecY *= fac; edgeVecZ *= fac;
			}*/
			
			// add together
			steerX = wanderVecX + separationVecX + cohesionVecX + alignVecX + latitudeVecX + edgeVecX;
			steerY = wanderVecY + separationVecY + cohesionVecY + alignVecY + latitudeVecY + edgeVecY;
			steerZ = wanderVecZ + separationVecZ + cohesionVecZ + alignVecZ + latitudeVecZ + edgeVecZ;
			
			/*if (boidId==0) {
				System.out.println("----------------------------------");
				System.out.println("pos: "+posX+", "+posY+", "+posZ);
				System.out.println("movement: "+movX+", "+movY+", "+movZ);
				System.out.println("neighbours: "+neighbourCount);
				for (int n=0;n<neighbourCount;n++) {
					System.out.println("neighbour "+n+" pos: "+x[n]+", "+y[n]+", "+z[n]);
				}
				System.out.println("average position of neighbours: "+px+", "+py+", "+pz);
				System.out.println("wander: "+wanderVecX+", "+wanderVecY+", "+wanderVecZ);
				System.out.println("cohesion: "+cohesionVecX+", "+cohesionVecY+", "+cohesionVecZ);
				System.out.println("align: "+alignVecX+", "+alignVecY+", "+alignVecZ);
			}*/
			
			//normalize steer to maxForce
			length = Math.sqrt(steerX*steerX + steerY*steerY + steerZ*steerZ);
			if (length!=0) {
				fac = maxForce / length;
				steerX *= fac; steerY *= fac; steerZ *= fac;
			}
			
			// add steering
			movX += steerX;
			movY += steerY;
			movZ += steerZ;
						
			// and normalize
			length = Math.sqrt(movX*movX + movY*movY + movZ*movZ);
			if (length!=0) {
				fac = speed / length;
				movX *= fac; movY *= fac; movZ *= fac;
			}
						
			// and update array
			xm[boidId] = movX;
			ym[boidId] = movY;
			zm[boidId] = movZ;
			
		}
		
		
		
		 
		 
		 
		
		
		
		
		
	}
	
	
	/**
	 * Returns an array of boidIds of length {@link #maxBoids}. The boidId=-1 signals the end of the list
	 * @param boidId
	 * @return
	 */
	public short[] getNeighbourIDsOfBoid(int boidId) {
		
		System.arraycopy(emptyNeighboursPerBoid, 0, neighboursPerBoid, 0, maxBoids);
		
		int neighbourCount = 0;
		int neighbourIndex = boidId*maxBoids;
		int neighbourIndexEnd = neighbourIndex+maxBoids;
		boolean moreBoids = true;
		while (neighbourIndex<neighbourIndexEnd && moreBoids) {
			short neighbourId = neighbourIdRegistry[neighbourIndex];
			if (neighbourId==-1) {
				moreBoids=false;
			} else {
				neighboursPerBoid[neighbourCount] = neighbourId;
				neighbourCount++;
			}
			neighbourIndex++;
		}
		
		return neighboursPerBoid;
		
	}
	
	
	
	
	
	public void newRandomMovementVectorAt(short boidIndex) {
		float x=0, y=0, z=0;
		double fac=0;
		while (fac==0) {
			x = (float)(speed * (Math.random()*2-1));
			y = (float)(speed * (Math.random()*2-1));
			z = (float)(speed * (Math.random()*2-1));
			fac = speed / Math.sqrt(x*x+y*y+z*z);
		}
		x*=fac;
		y*=fac;
		z*=fac;
		xm[boidIndex] = x;
		ym[boidIndex] = y;
		zm[boidIndex] = z;
		
		
	}
	
	
	/*public void addBoids(float[] x, float[] y, float[] z, float speed) {
		int amountToAdd = x.length;
		float[] xm = new float[amountToAdd];
		float[] ym = new float[amountToAdd];
		float[] zm = new float[amountToAdd];
		for (int i=0;i<amountToAdd;i++) {
			if (speed!=0) {
				double fac=0;
				while (fac==0 || Double.isNaN(fac) || Double.isInfinite(fac)) {
					xm[i] = (float)(speed*(Math.random()*2-1));
					ym[i] = (float)(speed*(Math.random()*2-1));
					zm[i] = (float)(speed*(Math.random()*2-1));
					fac = speed / Math.sqrt(xm[i]*xm[i]+ym[i]*ym[i]+zm[i]*zm[i]);
				}
				xm[i] *= fac;
				ym[i] *= fac;
				zm[i] *= fac;
			} else {
				xm[i]=0;
				ym[i]=0;
				zm[i]=0;
			}
		}
		addBoids(x, y, z, xm, ym, zm);
	}
	
	public void addBoids(float[] x, float[] y, float[] z, float[] xm, float[] ym, float[] zm) {
		int currentAmount = this.x.length;
		int amountToAdd = x.length;
		if (boidAmount+amountToAdd>maxBoids) {
			throw new RuntimeException("Not enough memory in spaceRegistry, too many boids");
		}
		//System.out.println("current amount"+currentAmount+", amountToAdd: "+amountToAdd);
		//System.out.println("xm: "+xm.length);
		increaseBoids(amountToAdd);
		//System.out.println(this.x.length);
		for (int i=0;i<amountToAdd;i++) {
			short index = (short)(currentAmount+i);
			this.id[index] = index;
			this.x[index] = x[i];
			this.y[index] = y[i];
			this.z[index] = z[i];
			this.xm[index] = xm[i];
			this.ym[index] = ym[i];
			this.zm[index] = zm[i];
			this.futureXm[index] = 0;
			this.futureYm[index] = 0;
			this.futureZm[index] = 0;
			this.wanderLat[index]=(float)(-maxLatitude+Math.random()*(2*maxLatitude));//-Math.PI/2.0+Math.random()*Math.PI);
			this.wanderLon[index]=(float)(Math.random()*Math.PI*2);
			//this.xm[index] = spee
		}
		
		updateInternalRegistry();
		//constrainSpeed();
	}*/
	
	
	/**
	 * V2 using {@link #maxLatitude}
	 */
	public void addBoids(float[] x, float[] y, float[] z, float speed) {
		int currentAmount = this.x.length;
		int amountToAdd = x.length;
		if (boidAmount+amountToAdd>maxBoids) {
			throw new RuntimeException("Not enough memory in spaceRegistry, too many boids");
		}
		//System.out.println("current amount"+currentAmount+", amountToAdd: "+amountToAdd);
		//System.out.println("xm: "+xm.length);
		increaseBoids(amountToAdd);
		//System.out.println(this.x.length);
		
		float xn=0, yn=0, zn=0;
		float COS, SIN;
		for (int i=0;i<amountToAdd;i++) {
			short index = (short)(currentAmount+i);
			this.id[index] = index;
			this.x[index] = x[i];
			this.y[index] = y[i];
			this.z[index] = z[i];
			this.lastX[index] = 0;
			this.lastY[index] = 0;
			this.lastZ[index] = 0;
			
			this.wanderLat[index]=(float)(-maxLatitude+Math.random()*(2*maxLatitude));//-Math.PI/2.0+Math.random()*Math.PI);
			this.wanderLon[index]=(float)(Math.random()*Math.PI*2);
			this.xm[index] = 0;
			this.ym[index] = 0;
			this.zm[index] = -speed; // go far away
			
			// rotate around x-axis (pitch): latitude;
			SIN=(float)Math.sin(wanderLat[index]); 
		    COS=(float)Math.cos(wanderLat[index]);
		    yn=ym[index]*COS-zm[index]*SIN;
		    zn=zm[index]*COS+ym[index]*SIN;
		    ym[index]=yn;
		    zm[index]=zn;
		    
		    // rotate around y-axis (yaw): longitude
		    SIN=(float)Math.sin(wanderLon[index]);
		    COS=(float)Math.cos(wanderLon[index]); 
		    xn=xm[index]*COS-zm[index]*SIN; 
		    zn=zm[index]*COS+xm[index]*SIN;
		    xm[index]=xn;
		    zm[index]=zn;
		}
		
		updateInternalRegistry();
		//constrainSpeed();
	}
	
	
	
	private void increaseBoids(int amount) {
		int size0 = boidAmount;
		int size1 = boidAmount+amount;
		boidAmount += amount;
		
		short[] newShortArr = new short[size1];
		System.arraycopy(id, 0, newShortArr, 0, size0);
		id = new short[size1];
		System.arraycopy(newShortArr, 0, id, 0, size0);
		
		int[] newIntArr = new int[size1];
		System.arraycopy(gridIndexX, 0, newIntArr, 0, size0);
		gridIndexX = new int[size1];
		System.arraycopy(newIntArr, 0, gridIndexX, 0, size0);
		
		System.arraycopy(gridIndexY, 0, newIntArr, 0, size0);
		gridIndexY = new int[size1];
		System.arraycopy(newIntArr, 0, gridIndexY, 0, size0);
		
		System.arraycopy(gridIndexZ, 0, newIntArr, 0, size0);
		gridIndexZ = new int[size1];
		System.arraycopy(newIntArr, 0, gridIndexZ, 0, size0);
		
		System.arraycopy(gridIndexAbsolute, 0, newIntArr, 0, size0);
		gridIndexAbsolute = new int[size1];
		System.arraycopy(newIntArr, 0, gridIndexAbsolute, 0, size0);
		
		System.arraycopy(color, 0, newIntArr, 0, size0);
		color = new int[size1];
		
		int minBrightness = 100;
		int maxBrightness = 400; // of max 255+255+255
		
		for (int i=size0;i<size1;i++) {
			
			int r=0, g=0, b=0;
			int brightness = (int)(minBrightness + Math.random()*maxBrightness);
			int chan;
			for (int c=0;c<brightness;c++) {
				chan = (int)(Math.random()*3);
				switch (chan) {
				case 0: r++; break;
				case 1: g++; break;
				case 2: b++; break;
				default: break;
				}
			}
			//int r = (int)(Math.random()*256);
			//int g = (int)(Math.random()*256);
			//int b = (int)(Math.random()*256);
			color[i] = 0xff<<24 | r<<16 | g<<8 | b;
		}
		System.arraycopy(newIntArr, 0, color, 0, size0);
		
		float[] newFloatArr = new float[size1];
		System.arraycopy(x, 0, newFloatArr, 0, size0);
		x = new float[size1];
		System.arraycopy(newFloatArr, 0, x, 0, size0);
		
		System.arraycopy(y, 0, newFloatArr, 0, size0);
		y = new float[size1];
		System.arraycopy(newFloatArr, 0, y, 0, size0);
		
		System.arraycopy(z, 0, newFloatArr, 0, size0);
		z = new float[size1];
		System.arraycopy(newFloatArr, 0, z, 0, size0);
		
		System.arraycopy(xm, 0, newFloatArr, 0, size0);
		xm = new float[size1];
		System.arraycopy(newFloatArr, 0, xm, 0, size0);
		
		System.arraycopy(ym, 0, newFloatArr, 0, size0);
		ym = new float[size1];
		System.arraycopy(newFloatArr, 0, ym, 0, size0);
		
		System.arraycopy(zm, 0, newFloatArr, 0, size0);
		zm = new float[size1];
		System.arraycopy(newFloatArr, 0, zm, 0, size0);
		
		System.arraycopy(lastX, 0, newFloatArr, 0, size0);
		lastX = new float[size1];
		System.arraycopy(newFloatArr, 0, lastX, 0, size0);
		
		System.arraycopy(lastY, 0, newFloatArr, 0, size0);
		lastY = new float[size1];
		System.arraycopy(newFloatArr, 0, lastY, 0, size0);
		
		System.arraycopy(lastZ, 0, newFloatArr, 0, size0);
		lastZ = new float[size1];
		System.arraycopy(newFloatArr, 0, lastZ, 0, size0);
				
		System.arraycopy(wanderLon, 0, newFloatArr, 0, size0);
		wanderLon = new float[size1];
		System.arraycopy(newFloatArr, 0, wanderLon, 0, size0);
		
		System.arraycopy(wanderLat, 0, newFloatArr, 0, size0);
		wanderLat = new float[size1];
		System.arraycopy(newFloatArr, 0, wanderLat, 0, size0);
		
		/*System.arraycopy(rotationX, 0, newFloatArr, 0, size0);
		rotationX = new float[size1];
		System.arraycopy(newFloatArr, 0, rotationX, 0, size0);
		
		System.arraycopy(rotationY, 0, newFloatArr, 0, size0);
		rotationY = new float[size1];
		System.arraycopy(newFloatArr, 0, rotationY, 0, size0);*/
		
	}

	public short getBoidAmount() {
		return boidAmount;
	}
	
}
