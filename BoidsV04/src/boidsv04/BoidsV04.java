package boidsv04;

import nonboid.Timer;
import nonboid.ValueInterpolator;
import processing.core.PApplet;
import processing.core.PFont;

public class BoidsV04 extends PApplet {

	final float speed = 1f;
	final int amount = 800;
	
	private static final long serialVersionUID = 7417955323989615724L;
	
	private Timer timer = new Timer(50);
	private ValueInterpolator vi;
	
	final int CAMERA_OUTSIDE = 0;
	final int CAMERA_LOOK_AT_BOID = 1;
	int cameraMode = CAMERA_OUTSIDE;
	
	BoidsWorld world = new BoidsWorld(300,120,210,10, (short)amount, speed);
	int worldWidth, worldHeight, worldDepth;
	
	boolean moveBoids = true;
	PFont font;
	
	boolean showHelp = true;
	short boidToFollow = 0;
	
	final char PAUSE_SIMULATION_KEY = 'p';
	
	int boidTracePointer = 0;
	float[] boidTraceXYZ = new float[3*300];
	
	float smooth = 0.97f;
	float invSmooth = 1-smooth;
	float[] lastMovX = new float[amount];
	float[] lastMovY = new float[amount];
	float[] lastMovZ = new float[amount];
	
	float[] avXm = new float[amount];
	float[] avYm = new float[amount];
	float[] avZm = new float[amount];
	//float[] smoothRotateX = new float[amount];
	//float[] smoothRotateY = new float[amount];
	
	public static void main(String[] args) {
		PApplet.main("boidsv04.BoidsV04");
	}
	
	public void settings() {
		size(1024,768,P3D);	
	}
	
	public void setup() {
		worldWidth = world.width;
		worldHeight = world.height;
		worldDepth = world.depth;
				
		timer.setPausing(true);
		
		font = loadFont("Calibri-Bold-24.vlw");
		textFont(font, 12);
		
		
		float[] x = new float[amount];
		float[] y = new float[amount];
		float[] z = new float[amount];
		for (int i=0;i<amount;i++) {
			x[i] = (float)(Math.random()*worldWidth);
			y[i] = (float)(Math.random()*worldHeight);
			z[i] = (float)(Math.random()*worldDepth);
			if (i==0) {
				x[i] = 150;
				y[i] = 50;
				z[i] = 170;
			}
		}
		world.addBoids(x, y, z, speed);
		world.printErrorMessages = true;
		
		world.wander = 1;
		world.cohesion = 1;
		world.separation = 1;
		world.alignment = 1;
		world.latitude = 0.15f;
		world.edgeAvoidance = 1.0f;
		world.edgeAvoidanceFac = 25.0f;
		world.setMaxLatitude(75*DEG_TO_RAD);
		world.maxForce = 0.05f;
		
		//world.wanderRadiansMax = 0.5f;
		
		vi = new ValueInterpolator(world.wander, world.cohesion, world.separation, world.alignment, world.edgeAvoidance);
		//world.
	}
	
	public void keyPressed() {
		if (key=='h') {
			showHelp = !showHelp;
		}
		if (key==' ') {
			switch (cameraMode) {
			case CAMERA_OUTSIDE:
				cameraMode = CAMERA_LOOK_AT_BOID;
				break;
			case CAMERA_LOOK_AT_BOID:
				cameraMode = CAMERA_OUTSIDE;
				break;
			default:
				break;
			}
		}
		if (key==PAUSE_SIMULATION_KEY) {
			moveBoids = !moveBoids;
		}
		float inc = 0.05f;
		if (key=='w') {
			world.wander = constrainDigits(world.wander,-inc,2);
			world.wander = Math.max(world.wander,0);
		}
		if (key=='W') {
			world.wander = constrainDigits(world.wander,inc,2);
			//world.wander = Math.min(world.wander,0.99f);
		}
		if (key=='s') {
			world.separation = constrainDigits(world.separation,-inc,2);
			world.separation = Math.max(world.separation,0);
		}
		if (key=='S') {
			world.separation = constrainDigits(world.separation,inc,2);
			//world.separation = Math.min(world.separation,0.99f);
		}
		if (key=='c') {
			world.cohesion = constrainDigits(world.cohesion,-inc,2);
			world.cohesion = Math.max(world.cohesion,0);
		}
		if (key=='C') {
			world.cohesion = constrainDigits(world.cohesion,inc,2);
			//world.cohesion = Math.min(world.cohesion,0.99f);
		}
		if (key=='a') {
			world.alignment = constrainDigits(world.alignment,-inc,2);
			world.alignment = Math.max(world.alignment,0);
		}
		if (key=='A') {
			world.alignment = constrainDigits(world.alignment,inc,2);
			//world.alignment = Math.min(world.alignment,0.99f);
		}
		if (key=='l') {
			world.latitude = constrainDigits(world.latitude,-inc,2);
			world.latitude = Math.max(0, world.latitude);
		}
		if (key=='L') {
			world.latitude = constrainDigits(world.latitude,inc,2);
		}
		if (key=='e') {
			world.edgeAvoidance = constrainDigits(world.edgeAvoidance,-inc,2);
			world.edgeAvoidance = Math.max(0, world.edgeAvoidance);
		}
		if (key=='E') {
			world.edgeAvoidance = constrainDigits(world.edgeAvoidance,inc,2);
		}
		if (key=='v') {
			interpolateNewValues();
		}
		if (key==CODED) {
			if (keyCode==RIGHT) {
				boidToFollow++;
				if (boidToFollow==world.getBoidAmount()) {
					boidToFollow=0;
				}
				setTraceToBoidToFollow();
			}
			if (keyCode==LEFT) {
				boidToFollow--;
				if (boidToFollow==-2) {
					boidToFollow = (short)(world.getBoidAmount()-1);
				}
				setTraceToBoidToFollow();
			}
		}
	}
	

	public void draw() {
		
		background(240,240,240);
		
		float max = Math.max(worldWidth, Math.max(worldHeight, worldDepth));
		float xo = (width/2-mouseX)*2f;
		float yo = (height/2-mouseY)*2f;
		
		
		boolean camFromOutside = (cameraMode==CAMERA_OUTSIDE || boidToFollow<0);
		
		if (camFromOutside) {
			camera(worldWidth/2+xo, worldHeight/2+yo, max, 
					worldWidth/2-xo, worldHeight/2-yo, worldDepth/2, 
					0, 1, 0);
		} else if (cameraMode==CAMERA_LOOK_AT_BOID){
			camera(worldWidth/2+xo, worldHeight/2+yo, max, 
					world.x[boidToFollow], world.y[boidToFollow], world.z[boidToFollow], 
					0, 1, 0);
		} else {
			camera();
		}
		
		
		drawWorldCube();
		drawBoidsExcept(boidToFollow);
		drawSingleBoidWithNeighbours(boidToFollow);
		
		drawOrientedBoid();
		//drawBoidGridCubes();
		//drawGridCubesFromRegistry();
		//appl
		
		
		drawStatistics();
		
		
		world.moveBoids(moveBoids);
		
		
		
		if (!timer.isPausing() && !timer.isFinished()) {
			float percentage = timer.update();
			float[] ip = vi.getInterpolatedValues(percentage);
			world.wander = ip[0];
			world.cohesion = ip[1];
			world.separation = ip[2];
			world.alignment = ip[3];
			world.edgeAvoidance = ip[4];
			//println("timer: "+percentage);
		}
	}
	
	private void interpolateNewValues() {
		timer.start();
		vi.startValues(world.wander, world.cohesion, world.separation, world.alignment, world.edgeAvoidance);
		float[] endValues = new float[5];
		for (int i=0;i<5;i++) endValues[i] = (float)Math.random();
		vi.endValues(endValues);
	}
	
	private void setTraceToBoidToFollow() {
		if (boidToFollow>=0) {
			for (int i=0;i<boidTraceXYZ.length;i+=3) {
				boidTraceXYZ[i] = world.x[boidToFollow];
				boidTraceXYZ[i+1] = world.y[boidToFollow];
				boidTraceXYZ[i+2] = world.z[boidToFollow];
			}
		}
	}

	private void drawStatistics() {
		camera();
		fill(128,0,0,192);
		noStroke();
		// display framerate and following boid
		String txt = "framerate: "+nf(frameRate,1,1)+"\n";
		if (boidToFollow>=0) {
			txt += "following boid nr: "+boidToFollow+"/"+world.getBoidAmount()+": ";
			//txt += nf(world.x[boidToFollow],1,2)+"/"+nf(world.y[boidToFollow],1,2)+"/"+nf(world.z[boidToFollow],1,2);
			txt += nf(world.x[boidToFollow],1,2)+"/"+nf(world.y[boidToFollow],1,2)+"/"+nf(world.z[boidToFollow],1,2)+"\n";
		} else {
			txt += "following no boid ("+world.getBoidAmount()+")\n";
		}
		// display wander and alignment
		txt += "\n";
		txt += "[w]/[W]ander: "+nf(world.wander,1,2)+"\n";
		txt += "[s]/[S]eparation: "+nf(world.separation,1,2)+"\n";
		txt += "[c]/[C]ohesion: "+nf(world.cohesion,1,2)+"\n";
		txt += "[a]/[A]lignment: "+nf(world.alignment,1,2)+"\n";
		txt += "[l]/[L]atitude: "+nf(world.latitude,1,2)+"\n";
		txt += "[e]/[E]dge avoidance: "+nf(world.edgeAvoidance,1,2)+"\n";
		
		txt += "\n[h] for help\n";
		if (showHelp) 
		{
			txt += "\n";
			txt += "[v]alue interpolator \n";
			txt += "["+PAUSE_SIMULATION_KEY+"] pause\n";
			txt += "[ARROW-LEFT] / [ARROW-RIGHT] to focus on other boid\n";
			txt += "[SPACE] change camera\n";
		}
		text(txt, 20, 20);
	}
	
	private float constrainDigits(float number, float numberAdd, int digits) {
		float fac = (float)Math.pow(10,digits);
		float nr = (number * fac);
		float nrAdd = (numberAdd * fac);
		int newNr = (int)Math.round(nr+nrAdd);
		return newNr/fac;
	}
	
	private void drawBoidsExcept(int except) {
		int amount = world.x.length;
		beginShape(POINTS);
		strokeWeight(3);
		for (int i=0;i<amount;i++) {
			if (i!=except) {
				stroke(world.color[i]);
				vertex(world.x[i], world.y[i], world.z[i]);
			}
		}
		endShape();
	}
	
	private void drawSingleBoid(int boidIndex, boolean asDot, boolean traceIt) {
		if (boidIndex<0) {
			return;
		}
		if (!asDot) {
			ellipseMode(CENTER);
			strokeWeight(1);
			pushMatrix();
			translate(world.x[boidIndex], world.y[boidIndex], world.z[boidIndex]);
			ellipse(0,0,5,5);
			popMatrix();
		} else {
			strokeWeight(12);
			stroke(world.color[boidIndex]);
			point(world.x[boidIndex], world.y[boidIndex], world.z[boidIndex]);
			
			strokeWeight(1);
			noFill();
			
			if (moveBoids) {
				boidTraceXYZ[boidTracePointer] = world.x[boidIndex];
				boidTraceXYZ[boidTracePointer+1] = world.y[boidIndex];
				boidTraceXYZ[boidTracePointer+2] = world.z[boidIndex];
			}
			
			int index = (boidTracePointer)%boidTraceXYZ.length;
			float lastX=boidTraceXYZ[index], lastY=boidTraceXYZ[index+1], lastZ=boidTraceXYZ[index+2];
			float dx, dy, dz;
			
			beginShape();
			for (int i=0;i<boidTraceXYZ.length/3;i+=3) {
				index = (i+boidTracePointer)%boidTraceXYZ.length;
				dx = lastX-boidTraceXYZ[index];
				dy = lastY-boidTraceXYZ[index+1];
				dz = lastZ-boidTraceXYZ[index+2];
				// over edge?
				if (Math.sqrt(dx*dx+dy*dy+dz*dz)>2*world.speed){
					endShape();
					beginShape();
				}
				
				
				vertex(boidTraceXYZ[index], boidTraceXYZ[index+1], boidTraceXYZ[index+2]);
				lastX=boidTraceXYZ[index];
				lastY=boidTraceXYZ[index+1];
				lastZ=boidTraceXYZ[index+2];
				
			}
			endShape();
			
			if (moveBoids) {
				boidTracePointer-=3;
				if (boidTracePointer<0) {
					boidTracePointer+=boidTraceXYZ.length;
				}
			}
		}
	}
	
	private void drawSingleBoidWithNeighbours(int boidIndex) {
		if (boidIndex<0) {
			return;
		}
		drawSingleBoid(boidIndex,true, true);
		
		short[] neighbours = world.getNeighbourIDsOfBoid(boidIndex);
		int i=0;
		while (i<neighbours.length && neighbours[i]!=-1) {
			drawSingleBoid(neighbours[i], false, false);
			i++;
		}
		
	}
	
	//TODO
	private void drawOrientedBoid() {
		short amount = (short)(world.x.length);
		
		if (frameCount==1) {
			System.arraycopy(world.xm, 0, lastMovX, 0, world.xm.length);
			System.arraycopy(world.ym, 0, lastMovY, 0, world.ym.length);
			System.arraycopy(world.zm, 0, lastMovZ, 0, world.zm.length);
			System.arraycopy(world.xm, 0, avXm, 0, world.xm.length);
			System.arraycopy(world.ym, 0, avYm, 0, world.ym.length);
			System.arraycopy(world.zm, 0, avZm, 0, world.zm.length);
		}
		
		for (short boid=0;boid<amount;boid++) {
			stroke(0);
			fill(128);
			pushMatrix();
			translate(world.x[boid], world.y[boid], world.z[boid]);//world.depth); //world.z[boid]
			
			// derive orientation from average movement:
			float xm = avXm[boid] = smooth*avXm[boid] + invSmooth*world.xm[boid];
			float ym = avYm[boid] = smooth*avYm[boid] + invSmooth*world.ym[boid];
			float zm = avZm[boid] = smooth*avZm[boid] + invSmooth*world.zm[boid];
			
			// derive orientation from movement
			/*float xm = world.xm[boid];
			float ym = world.ym[boid];
			float zm = world.zm[boid];*/
			
			// now normalize this movement         
			double len = Math.sqrt(xm*xm+ym*ym+zm*zm);
			if (len!=0) {
				double fac = 1.0/len;
				xm *= fac;
				ym *= fac;
				zm *= fac;
			}
			
			// now derive rotation
			float rotX = atan2(ym,zm);
			float rotY = PI + atan2(xm,zm);
			
			// and orient body from pointing into the distance to pointing at the path
			rotateY(rotY);
			if (zm<0) {
				rotateX(PI-rotX);
			} else {
				rotateX(rotX);
			}
			
			// ROLL:
			// angle between last and current movement on x-z plain
			float angle = (float)(Math.atan2(lastMovZ[boid], lastMovX[boid]) - Math.atan2(world.zm[boid], world.xm[boid]));
			rotateZ(angle);
			//atan2(v2.y,v2.x) - atan2(v1.y,v1.x)
			//rotateZ(frameCount*0.1f);
			
			scale(0.5f);
			beginShape(TRIANGLES);
			vertex(-5,0,5); //left
			vertex(5,0,5); //right
			vertex(0,0,-5); //nose
			
			vertex(-5,0,5); //left
			vertex(0,0,-5); //nose
			vertex(0,-5,5); // top;
			
			vertex(5,0,5); //right
			vertex(0,-5,5); // top;
			vertex(0,0,-5); //nose
			
			// wing
			//vertex(-5,0,5); //left
			//vertex(-15,0,5); //l-left
			//vertex(0,0,-5);// nose
			
			//vertex(5,0,5); //right
			//vertex(0,0,-5); //nose
			//vertex(15,0,5); // r-right
			
			endShape();
			popMatrix();
			
			
		}
		
		System.arraycopy(world.xm, 0, lastMovX, 0, world.xm.length);
		System.arraycopy(world.ym, 0, lastMovY, 0, world.ym.length);
		System.arraycopy(world.zm, 0, lastMovZ, 0, world.zm.length);
	}
	
	
	private void drawWorldCube() {
		noFill();
		stroke(0,0,0);
		strokeWeight(1);
		beginShape(QUADS);
		//back
		vertex(0,0,0);
		vertex(worldWidth, 0, 0);
		vertex(worldWidth, worldHeight, 0);
		vertex(0, worldHeight, 0);
		// front
		vertex(0,0,worldDepth);
		vertex(worldWidth, 0, worldDepth);
		vertex(worldWidth, worldHeight, worldDepth);
		vertex(0, worldHeight, worldDepth);
		endShape();
	}
	
	
}
