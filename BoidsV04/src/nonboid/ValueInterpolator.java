package nonboid;

public class ValueInterpolator {

	private float[] v0;
	private float[] dv;
	private float[] out;
	
	private final int amount;
	
	public ValueInterpolator(float... initialValues) {
		amount = initialValues.length;
		v0 = new float[amount];
		dv = new float[amount];
		out = new float[amount];
		startValues(initialValues);
	}
	
	public void startValues(float... initialValues) {
		if (initialValues.length!=amount) {
			throw new RuntimeException("Expected "+amount+" values, but got "+initialValues.length);
		}
		System.arraycopy(initialValues, 0, v0, 0, amount);
	}
	
	public void endValues(float... endValues) {
		if (endValues.length!=amount) {
			throw new RuntimeException("Expected "+amount+" values, but got "+endValues.length);
		}
		for (int i=0;i<amount;i++) {
			dv[i] = endValues[i] - v0[i];
		}
	}
	
	public float[] getInterpolatedValues(float p) {
		for (int i=0;i<amount;i++) {
			out[i] = v0[i]+p*dv[i];
		}
		return out;
	}
	
}
