package nonboid;

public class Timer {

	private boolean finished = true;
	private boolean pausing = false;
	private int maxFrames = 0;
	private int frames = 0;
	
	public Timer(int frames) {
		maxFrames = Math.abs(frames);
		start();
	}
	
	public void setPausing(boolean p) {
		pausing = p;
	}
	
	public boolean isPausing() {
		return pausing;
	}
	
	public void start() {
		frames = maxFrames;
		finished = false;
		pausing = false;
	}
	
	public float update() {
		if (!finished && !pausing) {
			frames--;
		}
		if (frames==0) {
			finished = true;
		}
		return getPercentage();
	}
	
	public float getPercentage() {
		return 1.0f-(float)frames/(float)maxFrames;
	}
	
	public boolean isFinished() {
		return finished;
	}
	
}
