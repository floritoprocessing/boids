class Car {

  float lastX, lastY;
  float x0, x1, y0, y1;
  boolean lastOverEdge = true;
  boolean overEdge = true;
  
  PMatrix3D totalMatrix = new PMatrix3D();
  PMatrix3D speed = new PMatrix3D();
  //float heading = random(-TWO_PI,TWO_PI);

  Car(float x, float y) {
    totalMatrix.translate(x, y);
    totalMatrix.rotate(random(0,TWO_PI));
    speed.translate(2,0);
    
    float offEdge = 12;
    x0 = -offEdge;
    x1 = width+offEdge;
    y0 = -offEdge;
    y1 = height+offEdge;
    
  }
  
  float noiseFunc(float x, float y) {
    float n = noise(x*0.01,y*0.01);
    float r = random(1.0);
    float rnd = mix(n,r,0.7);
    return rnd;
  }
  

  void draw(PGraphics pg, boolean showCar) {

    //PMatrix3D rotation = new PMatrix3D();
    //float dir = random(1.0)<0.5 ? -0.1 : 0.1;
    //rotation.rotate(radians(dir));

    float x = totalMatrix.m03;
    float y = totalMatrix.m13;
    float n = noiseFunc(x,y);//noise(x,y);
    float dirChange = 0.02*map(n,0,1,-TWO_PI,TWO_PI);

    totalMatrix.apply(speed);
    totalMatrix.rotate(dirChange);
    

    //direction.apply(rotation);
    //position.apply(direction);

    if (showCar) {
      pushMatrix();
      applyMatrix(totalMatrix);
      rectMode(CENTER);
      rect(0, 0, 10, 10);
      rect(7.5, 0, 5, 5);
      popMatrix();
    }
    
    
    overEdge = false;
    if (x<x0) {
      totalMatrix.m03 = x + (x1-x0);
      overEdge = true;
    } else if (x>=x1) {
      totalMatrix.m03 = x - (x1-x0);
      overEdge = true;
    }
    if (y<y0) {
      totalMatrix.m13 = y + (y1-y0);
      overEdge = true;
    } else if (y>=y1) {
      totalMatrix.m13 = y - (y1-y0);
      overEdge = true;
    }
    
    
    if (!lastOverEdge) {
      pg.line(lastX,lastY,x,y);
      
    }
    //overEdge = false;
    
    lastX = x;
    lastY = y;
    
    lastOverEdge = overEdge;
    
  }
}

