Car[] car = new Car[200];
PImage bg;
PGraphics pg;

void setup() {
  size(800,600,P3D);
  
  pg = createGraphics(width,height);
  
  for (int i=0;i<car.length;i++) {
    car[i] = new Car(random(width),random(height));
  }
  
  bg = new PImage(width,height);
  int i=0;
  float n, r, g;
  for (int y=0;y<height;y++) {
    for (int x=0;x<width;x++) {
      n = car[0].noiseFunc(x,y);
      r = g = 0;
      if (n<0.5) {
        g = map(n,0,0.5,255,0);
      } else {
        r = map(n,0.5,1.0,0,255);
      }
      bg.pixels[i] = color(r,g,0);
      i++;
    }
  }
  bg.updatePixels();
  background(0);
}

void draw() {
  if (mousePressed && mouseButton==LEFT) background(bg); else background(0);
  image(pg,0,0);
  pg.beginDraw();
  pg.smooth();
  pg.blendMode(ADD);
  pg.stroke(16,40,255,32);
  for (int i=0;i<car.length;i++) {
    car[i].draw(pg, !(mousePressed && mouseButton==RIGHT));
  }
  pg.endDraw();
}


static float mix(float v0, float v1, float p0) {
  return v0*p0 + v1*(1-p0);
}
