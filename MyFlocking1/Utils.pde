

//public void limit(float max) {
//  if (mag() > max) {
//    normalize();
//    mult(max);
//  }
//}

void limit(PVector v, float max) {

  float xx = v.x*v.x;
  float yy = v.y*v.y;
  float magSq = xx+yy;
  float maxSq = max*max;
  if (magSq>maxSq) {

    float mag = sqrt(magSq);
    float fac = max/mag;
    v.x *= fac;
    v.y *= fac;
  }
}

