// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// Demonstration of Craig Reynolds' "Flocking" behavior
// See: http://www.red3d.com/cwr/
// Rules: Cohesion, Separation, Alignment

// Click mouse to add boids into the system

Flock flock;

void setup() {
  size(displayWidth, displayHeight, P2D);
  flock = new Flock();
  // Add an initial set of boids into the system
  for (int i = 0; i < 200; i++) {
    Boid b = new Boid(width/2,height/2, random(-1,1),random(-1,1));
    flock.addBoid(b);
  }
  smooth();
}

void draw() {
  background(0);
  flock.run();
  
    // Instructions
  fill(255);
  text(flock.boids.size()+" boids at "+nf(frameRate,1,1)+"fps",10,20);
  text("flockTime="+flock.runTime+" ms",10,35);
  //text("Drag the mouse to generate new boids.",10,height-16);
}

// Add a new boid into the System
void mouseDragged() {
  
  float d = dist(pmouseX,pmouseY,mouseX,mouseY);
  d = max(1,d);
  float stepSize = 3;
  float mx = (mouseX-pmouseX);///d;
  float my = (mouseY-pmouseY);///d;
  for (int step=0;step<d;step+=stepSize) {
    float p = map(step,0,d,0,1);
    float x = map(p,0,1,pmouseX,mouseX);
    float y = map(p,0,1,pmouseY,mouseY);
    flock.addBoid(new Boid(x,y,mx,my));  
  }
  
  
  
  println(flock.boids.size());
}


