// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// Boid class
// Methods for Separation, Cohesion, Alignment added

final float SEPERATION_DISTANCE = 25;
final float SEPERATION_DISTANCE_SQ = SEPERATION_DISTANCE*SEPERATION_DISTANCE;
final float ALIGN_DISTANCE = 50;
final float ALIGN_DISTANCE_SQ = ALIGN_DISTANCE*ALIGN_DISTANCE;
final float COHESION_DISTANCE = 50;
final float COHESION_DISTANCE_SQ = COHESION_DISTANCE*COHESION_DISTANCE;
final float REG_DISTANCE = max(SEPERATION_DISTANCE, max(ALIGN_DISTANCE, COHESION_DISTANCE));



class Boid {

  PVector wanderVec;
  PVector location;
  PVector velocity;
  PVector acceleration;
  float r;
  float maxforce;    // Maximum steering force
  float maxspeed;    // Maximum speed

    Boid(float x, float y, float xm, float ym) {
    acceleration = new PVector(0, 0);
    velocity = new PVector(xm, ym);
    wanderVec = new PVector(random(-1, 1), random(-1, 1));
    wanderVec.normalize();
    location = new PVector(x, y);
    r = 3.0;
    maxspeed = 3;
    maxforce = 0.05;
  }



  void run(ArrayList<Boid> boids, Registry registry) {
    flock(boids, registry);
    update();
    borders();
    render();
  }
  
  

  void applyForce(PVector force) {
    // We could add mass here if we want A = F / M
    acceleration.add(force);
  }



  // We accumulate a new acceleration each time based on three rules
  void flock(ArrayList<Boid> boids, Registry registry) {

    PVector wan = wander(wanderVec);
    PVector sep = separate(registry);   // Separation
    PVector ali = align(registry);      // Alignment
    PVector coh = cohesion(registry);   // Cohesion

    // Arbitrarily weight these forces
    wan.mult(0.05);
    sep.mult(1.5);
    ali.mult(1.0);
    coh.mult(1.0);

    // Add the force vectors to acceleration
    applyForce(wan);
    applyForce(sep);
    applyForce(ali);
    applyForce(coh);
  }





  // Method to update location
  void update() {
    // Update velocity
    velocity.add(acceleration);
    // Limit speed
    limit(velocity, maxspeed); //velocity.limit(maxspeed);
    location.add(velocity);
    // Reset accelertion to 0 each cycle
    acceleration.mult(0);
  }




  // A method that calculates and applies a steering force towards a target
  // STEER = DESIRED MINUS VELOCITY
  PVector seek(PVector target) {
    PVector desired = PVector.sub(target, location);  // A vector pointing from the location to the target
    // Normalize desired and scale to maximum speed
    desired.normalize();
    desired.mult(maxspeed);
    // Steering = Desired minus Velocity
    PVector steer = PVector.sub(desired, velocity);
    // Limit to maximum steering force
    limit(steer, maxforce); //steer.limit(maxforce); 
    return steer;
  }




  void render() {
    // Draw a triangle rotated in the direction of velocity
    float theta = velocity.heading2D() + radians(90);
    fill(175);
    stroke(0);
    pushMatrix();
    translate(location.x, location.y);
    rotate(theta);
    beginShape(TRIANGLES);
    vertex(0, -r*2);
    vertex(-r, r*2);
    vertex(r, r*2);
    endShape();
    popMatrix();
  }





  // Wraparound
  void borders() {
    if (location.x < -r) location.x = width+r;
    if (location.y < -r) location.y = height+r;
    if (location.x > width+r) location.x = -r;
    if (location.y > height+r) location.y = -r;
  }





  PVector wander(PVector w) {

    PVector n = new PVector(random(-1, 1), random(-1, 1));
    w.x = 0.9*w.x + 0.1*n.x;
    w.y = 0.9*w.y + 0.1*n.y;
    w.normalize();
    return w;
  }





  // Separation
  // Method checks for nearby boids and steers away
  // ArrayList<Boid> boids, 
  PVector separate (Registry registry) {

    PVector steer = new PVector(0, 0, 0);
    int count = 0;
    PVector ol;
    float dx, dy;

    ArrayList<Boid> others = registry.getNeighboursFor(this);

    if (others!=null) {
      for (Boid other : others) {

        ol = other.location;
        dx = ol.x-location.x;
        dy = ol.y-location.y;
        if (Math.abs(dx)+Math.abs(dy)<SEPERATION_DISTANCE) {

          float dSQ = dx*dx+dy*dy;

          // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
          if ((dSQ > 0) && (dSQ < SEPERATION_DISTANCE_SQ)) {
            float d = sqrt(dSQ);
            // Calculate vector pointing away from neighbor
            PVector diff = PVector.sub(location, other.location);
            diff.normalize();
            diff.div(d);        // Weight by distance
            steer.add(diff);
            count++;            // Keep track of how many
          }
        }
      }
    }
    // Average -- divide by how many
    if (count > 0) {
      steer.div((float)count);
    }

    // As long as the vector is greater than 0

    float magSq = steer.x*steer.x+steer.y*steer.y;

    if (magSq>0) { //if (steer.mag() > 0) {
      // Implement Reynolds: Steering = Desired - Velocity

      //steer.normalize();
      //steer.mult(maxspeed);
      float fac = maxspeed / sqrt(magSq);
      steer.x *= fac;
      steer.y *= fac;

      steer.sub(velocity);

      limit(steer, maxforce); //steer.limit(maxforce);
    }
    return steer;
  }






  // Alignment
  // For every nearby boid in the system, calculate the average velocity
  // ArrayList<Boid> boids, 
  PVector align (Registry registry) {

    PVector sum = new PVector(0, 0);
    int count = 0;
    PVector ol;
    float dx, dy;

    ArrayList<Boid> others = registry.getNeighboursFor(this);

    if (others!=null) {
      for (Boid other : others) {

        ol = other.location;
        dx = ol.x-location.x;
        dy = ol.y-location.y;
        if (Math.abs(dx)+Math.abs(dy)<ALIGN_DISTANCE) {

          float dSQ = dx*dx+dy*dy;


          if ((dSQ > 0) && (dSQ < ALIGN_DISTANCE_SQ)) {
            sum.add(other.velocity);
            count++;
          }
        }
      }
    }
    if (count > 0) {
      sum.div((float)count);
      sum.normalize();
      sum.mult(maxspeed);
      PVector steer = PVector.sub(sum, velocity);
      limit(steer, maxforce); //steer.limit(maxforce);
      return steer;
    } else {
      return new PVector(0, 0);
    }
  }







  // Cohesion
  // For the average location (i.e. center) of all nearby boids, calculate steering vector towards that location
  //ArrayList<Boid> boids, 
  PVector cohesion (Registry registry) {

    PVector sum = new PVector(0, 0);   // Start with empty vector to accumulate all locations
    int count = 0;
    PVector ol;
    float dx, dy;

    ArrayList<Boid> others = registry.getNeighboursFor(this);

    if (others!=null) {
      for (Boid other : others) {

        ol = other.location;
        dx = ol.x-location.x;
        dy = ol.y-location.y;
        if (Math.abs(dx)+Math.abs(dy)<COHESION_DISTANCE) {

          float dSQ = dx*dx+dy*dy;


          if ((dSQ > 0) && (dSQ < COHESION_DISTANCE_SQ)) {
            sum.add(other.location); // Add location
            count++;
          }
        }
      }
    }
    if (count > 0) {
      sum.div(count);
      return seek(sum);  // Steer towards the location
    } else {
      return new PVector(0, 0);
    }
  }
}

