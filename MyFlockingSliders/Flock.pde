// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// Flock class
// Does very little, simply manages the ArrayList of all the boids

class Flock {
  
  ArrayList<Boid> boids; // An ArrayList for all the boids
  Registry registry;

  Flock() {
    boids = new ArrayList<Boid>(); // Initialize the ArrayList
    registry = new Registry(width, height, REG_DISTANCE);
  }

  void run() {
    registry.reset();
    for (Boid b : boids) {
      registry.register(b);
    }
    registry.buildNeighbours();
    
    
    for (Boid b : boids) {
      b.run(boids, registry);  // Passing the entire list of boids to each boid individually
    }
  }

  void addBoid(Boid b) {
    boids.add(b);
  }

}
