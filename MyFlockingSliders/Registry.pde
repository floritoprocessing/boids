class Registry {
  
  int regWidth;
  int regHeight;
  int regSize;
  
  int screenWidth;
  int screenHeight;
  float distance;
  
  float screenToIndex;
  
  RegistryCell[] reg;
  
  Registry(int screenWidth, int screenHeight, float distance) {
    this.screenWidth = screenWidth;
    this.screenHeight = screenHeight;
    this.distance = distance;
    this.screenToIndex = 1 / distance;
    this.regWidth = int(screenWidth*screenToIndex)+1;
    this.regHeight = int(screenHeight*screenToIndex)+1;
    this.regSize = regWidth*regHeight;
    this.reg = new RegistryCell[regWidth*regHeight];
    for (int i=0;i<this.regSize;i++) {
      reg[i] = new RegistryCell();
    }
    println("Registry with "+screenWidth+"x"+screenHeight+" and distance "+distance+" -> "+regWidth+"x"+regHeight);
  }
  
  void reset() {
    for (int i=0;i<regSize;i++) {
      reg[i].clear();
    }
  }
  
  void register(Boid boid) {
    PVector loc = boid.loc;
    int x = (int)(loc.x*screenToIndex);
    int y = (int)(loc.y*screenToIndex);
    if (x>=0&&x<regWidth&&y>=0&&y<regHeight) {
      int index = y * regWidth + x;
      reg[index].add(boid);
    }
  }
  
  // creates a full neighbourhood for each cell
  void buildNeighbours() {
    
    int cellIndex = 0;
    int x, y, xi, yi;
    int xx, yy;
    RegistryCell currentCell;
    RegistryCell neighbourCell;
    int nIndex;
    
    for (y=0;y<regHeight;y++) {
      for (x=0;x<regWidth;x++) {
      
        currentCell = reg[cellIndex];
        currentCell.neighbours.clear();
        
        // per cell:
        for (yi=-1;yi<=1;yi++) {
          for (xi=-1;xi<=1;xi++) {
            
            xx = x+xi;
            yy = y+yi;
            if (xx>=0&&xx<regWidth&&yy>=0&&yy<regHeight) {
              
              nIndex = yy*regWidth + xx;
              neighbourCell = reg[nIndex];
              currentCell.neighbours.addAll(neighbourCell);
              
            } 
            
            
          }
        }
      
      
        cellIndex++;
      }
    }
    
  }
  
  
  
  /**
  *  Gets all neighbours INCLUDING ITSELF!!!!!!
  */  
  ArrayList<Boid> getNeighboursFor(Boid boid)
  {
     PVector loc = boid.loc;
     int x = (int)(loc.x*screenToIndex);
     int y = (int)(loc.y*screenToIndex);
     if (x>=0&&x<regWidth&&y>=0&&y<regHeight) {
       return reg[y*regWidth+x].neighbours;
     } else {
       return null;
     }
  }
  
  
}
