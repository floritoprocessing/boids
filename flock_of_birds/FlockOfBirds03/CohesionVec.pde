/*
Cohesion steering behavior gives an character the ability to cohere with (approach and form a group with) 
other nearby characters. See Figure 15. 
Steering for cohesion can be computed by finding all characters in the local neighborhood 
(as described above for separation), 
computing the �average position� (or �center of gravity�) of the nearby characters. 
The steering force can applied in the direction of that �average position� 
(subtracting our character position from the average position, as in the original boids model), 
or it can be used as the target for seek steering behavior.
*/

class CohesionVec extends Vec {
  CohesionVec() {
  }
  
  void update(Vector neighbours, Vec pos) {
    
    if (neighbours.size()>0) {
      Vec averagePos=new Vec();
      for (int i=0;i<neighbours.size();i++) {
        averagePos.add(((Bird)neighbours.elementAt(i)).pos);
      }
      averagePos.div((double)neighbours.size());
      setVec(vecSub(averagePos,pos));
    } else {
      setVec(new Vec(0,0,0));
    }
  }
}
