class WanderVec extends Vec {
  double lat=-Math.PI/2.0+Math.random()*Math.PI;
  double lon=Math.random()*Math.PI*2;
  double sphereRadius=1;
  float rdMax=0.2;
  Vec offVec;
  
  void setSphereRadius(double sr) {
    sphereRadius=sr;
  }
  
  void update(Vec v) {
    offVec=new Vec(v);
    offVec.setLen(sphereRadius);
//    offVec=new Vec(0,0,sphereRadius);
    
    lat+=random(-rdMax,rdMax);
    while (lat<-Math.PI/2.0) {lat+=Math.PI;}
    offVec.rotX(lat);
    
    lon+=random(-rdMax,rdMax);
    while (lon<0) {lon+=(2*Math.PI);}
    offVec.rotY(lon);
    
    setVec(v);
    add(offVec);
  }
}
