/************************************************************************************************
 * 
 * SpacialData
 * 
 * divides the total space into quadrants with the side length of the bird's perception radius
 *
 * requirements:
 * an Object gives a spacial position, SpacialData returns all Birds within its and its neigboughring quadrants
 * how to do this?
 * 
 * 
 *************************************************************************************************/

class SpacialData {
  int nrX=0, nrY=0, nrZ=0;
  int nrOfSpaceBoxes=0;
  double loX=0, loY=0, loZ=0;
  double side=0;

  // each spaceBox holds the reference to a number of birds
  Vector[][][] spaceBox;

  // this vector holds all the birds in the possible range
  Vector birdsInPossibleRange=new Vector(); 

  SpacialData() {
  }

  void init(double rx, double ry, double rz, double r) {
    loX=-rx/2.0; 
    loY=-ry/2.0; 
    loZ=-rz/2.0;
    if (Math.floor(rx/r)==rx/r) {
      nrX=(int)(rx/r);
    } 
    else {
      nrX=(int)(rx/r)+1;
    }
    if (Math.floor(ry/r)==ry/r) {
      nrY=(int)(ry/r);
    } 
    else {
      nrY=(int)(ry/r)+1;
    }
    if (Math.floor(rz/r)==rz/r) {
      nrZ=(int)(rz/r);
    } 
    else {
      nrZ=(int)(rz/r)+1;
    }

    nrOfSpaceBoxes=nrX*nrY*nrZ;
    side=r;
    println("Spacial Data grid is: "+nrX+","+nrY+","+nrZ);

    clear();
  }

  void clear() {
    spaceBox=new Vector[nrX][nrY][nrZ];
    for (int x=0;x<nrX;x++) {
      for (int y=0;y<nrY;y++) {
        for (int z=0;z<nrZ;z++) {
          spaceBox[x][y][z]=new Vector();
        }
      }
    }
  }

  void addBird(Bird b) {
    Vec pos=new Vec(b.pos);
    int x,y,z;
    x=(int)Math.floor((pos.x-loX)/side);
    y=(int)Math.floor((pos.y-loY)/side);
    z=(int)Math.floor((pos.z-loZ)/side);
    spaceBox[x][y][z].addElement(b);
  }


  Vec getBoxNrFromPosition(Vec pos) {
    double outX=(pos.x-loX)/side;
    double outY=(pos.y-loY)/side;
    double outZ=(pos.z-loZ)/side;
    return new Vec(Math.floor(outX),Math.floor(outY),Math.floor(outZ));
  }

  Vec getPosOfBoxFromNr(Vec pos) {
    double x=loX+(int)pos.x*side+0.5*side;
    double y=loY+(int)pos.y*side+0.5*side;
    double z=loZ+(int)pos.z*side+0.5*side;
    return new Vec(x,y,z);
  }

  // start setBirdsInPossibleRange --------------------------------------------------------
  void setBirdsInPossibleRange(Bird bird) {
    // clear this vector:
    birdsInPossibleRange.clear();

    // get the x/y/z index from the box the bird is in
    Vec boxIndex=new Vec(getBoxNrFromPosition(bird.pos));

    // check all boxes around the box that the bird is in:
    for (int x=(int)boxIndex.x-1;x<=(int)boxIndex.x+1;x++) {
      for (int y=(int)boxIndex.y-1;y<=(int)boxIndex.y+1;y++) {
        for (int z=(int)boxIndex.z-1;z<=(int)boxIndex.z+1;z++) {


          if (x>=0&&x<nrX&&y>=0&&y<nrY&&z>=0&&z<nrZ) {
            // add all the birds that are in a box (except for itself)
            for (int i=0;i<spaceBox[x][y][z].size();i++) {
              if (bird.getIndex()!=((Bird)spaceBox[x][y][z].elementAt(i)).getIndex()) {
                birdsInPossibleRange.addElement(spaceBox[x][y][z].elementAt(i));
              }
            }
          }

          boolean drawIt=false;
          if (drawIt) {
            drawLocalBox(x,y,z);
          }
        }
      }
    }
  }
  // end setBirdsInPossibleRange --------------------------------------------------------
  
  void setBirdsInRange(Bird bird) {
    setBirdsInPossibleRange(bird);
    bird.neighbourBirds.clear();
    for (int i=0;i<birdsInPossibleRange.size();i++) {
      double distance;
      Bird targetBird=(Bird)birdsInPossibleRange.elementAt(i);
      distance = vecLen(vecSub(bird.pos,targetBird.pos));
      if (distance>bird.getPerceptionRadius()) {
        birdsInPossibleRange.remove(targetBird);
      } else {
        bird.neighbourBirds.addElement(targetBird);
      }
    }
    
    
  }

  void drawLocalBox(double _x, double _y, double _z) {
    Vec tempIndex=new Vec(_x,_y,_z);
    if (_x>=0&&_x<nrX&&_y>=0&&_y<nrY&&_z>=0&&_z<nrZ) {
      Vec boxCenter=new Vec(getPosOfBoxFromNr(tempIndex));
      noFill();
      stroke(128,255,128);
      pushMatrix();
      vecTranslate(boxCenter);
      box((float)side);
      popMatrix();
    }
  }
}
