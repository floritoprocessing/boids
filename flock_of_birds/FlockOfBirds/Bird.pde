/********************************************************************************
 * class Birds
 * 
 * a bird has a direction and a speed
 * a bird has three behaviours:
 * 
 * (1) Separation: steer to avoid crowding local flockmates
 * (2) Alignment: steer towards the average heading of local flockmates 
 * (3) Cohesion: steer to move toward the average position of local flockmates 
 * 
 *********************************************************************************/


// constant:
int BIRD_BOUND_BEHAVE_CLIP=1;

class Bird {
  int index=0;
  double perceptionRadius=0;
  Vec pos=new Vec();
  Vec mov=new Vec();
  double maxVel=0;
  double ALIGN_STRENGTH=0.2;
  
  Vector neighbourBirds=new Vector();
  
  double boundX1, boundX2, boundY1, boundY2, boundZ1, boundZ2;
  double boundXR, boundYR, boundZR;
  int boundBehaviour=0;

  Bird() {
  }
  
  Bird(Vec _pos) {
    pos=_pos;
  }
  
  Bird(Vec _pos, Vec _mov) {
    pos=_pos;
    mov=_mov;
  }
  
  void setIndex(int i) {
    index=i;
  }
  
  void setPerceptionRadius(double _r) {
    perceptionRadius=_r;
  }
  
  double getPerceptionRadius() {
    return perceptionRadius;
  }
  
  void setPos(Vec v) {
    pos=new Vec(v);
  }
  
  void setMov(Vec v) {
    mov=new Vec(v);
  }
  
  void setMaxVel(double mv) {
    maxVel=mv;
  }
  
  void setBoundaries(double xr, double yr, double zr) {
    boundXR=xr; boundYR=yr; boundZR=zr;
    boundX1=-xr/2.0; boundX2=xr/2.0;
    boundY1=-yr/2.0; boundY2=yr/2.0;
    boundZ1=-zr/2.0; boundZ2=zr/2.0;
  }
  
  void setBoundBehaviour(int _b) {
    boundBehaviour=_b;
  }

  int getIndex() {
    return index;
  }
  
  Vec getPos() {
    return new Vec(pos); 
  }
  
  Vec getMov() {
    return new Vec(mov); 
  }
  
  double getVel() {
    return mov.len();
  }
  
  
  
  void updatePosFromFlock() {
    //   Separation: steer to avoid crowding local flockmates  
    //   Alignment: steer towards the average heading of local flockmates  
    //   Cohesion: steer to move toward the average position of local flockmates 
    
    
    
/*    // Alignment:
    Vec averageMov=new Vec(mov);
    int nrOfNeighbours=neighbourBirds.size();
    if (nrOfNeighbours>0) {
      averageMov=new Vec();
      for (int i=0;i<nrOfNeighbours;i++) {
        averageMov.add(((Bird)neighbourBirds.elementAt(i)).mov);
      }
      averageMov.div((double)nrOfNeighbours);
    }
    mov=vecAdd(vecMul(mov,1.0-ALIGN_STRENGTH),vecMul(averageMov,ALIGN_STRENGTH));
//    mov.toLen(maxVel);
    mov.toAvLen(maxVel); // set length to average between this and target length
    */    
    
    
    // find average direction of surrounding birds
    Vec averageDir=new Vec();
    int nrOfNeighbours=neighbourBirds.size();
    if (nrOfNeighbours>0) {
      averageDir=new Vec();
      for (int i=0;i<nrOfNeighbours;i++) {
        averageDir.add(((Bird)neighbourBirds.elementAt(i)).mov);
      }
      averageDir.div((double)nrOfNeighbours);
    }
    
    // apply steering force in the direction 
    // which is the difference between the current velocity and the desired velocity 
    // towards target
    if (!averageDir.isNullVec()) {
      // if there is any bird in the neighbourhood:
      //double currentVel=mov.len();
      averageDir.toLen(maxVel);
      mov.sub(averageDir);
      //mov.add(steeringForce); 
      //mov=steeringForce;
    }
  }
  
  void updatePos() {
    pos.add(mov);
    if (boundBehaviour==BIRD_BOUND_BEHAVE_CLIP) {
      while (pos.x<=boundX1) {pos.x+=boundXR;}
      while (pos.x>boundX2) {pos.x-=boundXR;}
      while (pos.y<=boundY1) {pos.y+=boundYR;}
      while (pos.y>boundY2) {pos.y-=boundYR;}
      while (pos.z<=boundZ1) {pos.z+=boundZR;}
      while (pos.z>boundZ2) {pos.z-=boundZR;}
    }
  }
}



