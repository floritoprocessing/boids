/***************************************
 * 
 *    Birds01
 *    see http://www.red3d.com/cwr/boids/
 * 
 *    also check http://www.red3d.com/cwr/steer/gdc99/
 *    When the birds positions are updated, the SpacialDataStructure gets updated as well.
 *    This was, the finding of neighbours is easier
 * 
 ***************************************/

import java.util.Vector;

Bird[] bird=new Bird[40];
SpacialData spacialData=new SpacialData();

// system variables:
double BIRD_MAX_VELOCITY=10;         // maximum velocity
double BIRD_INIT_POS_BOX_SIDE=200;  // initial random x/y/z coordinate box [-side,side]
double BIRD_PERCEPTION_RADIUS=150;  // local perception of bird

double BOUNDARIES_X_RANGE=1000;      // min/max x pos
double BOUNDARIES_Y_RANGE=750;       // min/max y pos
double BOUNDARIES_Z_RANGE=1000;      // min/max z pos
boolean BOUNDARIES_DRAW=true;        // show bounding box

// display variables
double BIRD_SIZE=4;            // sphere representing the bird
color BIRD_COLOR=#FF8080;      // color of sphere
color BIRD_MOV_COLOR=#0000FF;  // color of movement vector
boolean BIRD_PERCEPTION_RADIUS_DRAW=false; // draw radius of perception
color BIRD_LOCAL_COLOR=#000088;// color of local perception
double BIRD_MOV_SHOW_EXAG=5;   // exaggeration of display of mov vector
color BOUNDARIES_COLOR=#00FF00;// color of bounding box
color SPACIAL_DATA_COLOR=#88FF88;

void setup() {
  size(768,576,P3D);
  initBirds();
  initSpacialData();
}

void draw() {
  background(240,240,255);
  translate(width/2,height/2,(float)-BOUNDARIES_Z_RANGE/2.0+200);
  if (mousePressed) {
    initBirds();
  }

  updateBirdsInPossibleRange();
  showBirdsInRange();
  updatePosFromFlock();


  // new position:
  posUpdateBirds();
  updateSpacialData();

  drawBoundaries();
  drawBirds();
}

// INIT FUNCTIONS:

void initBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i]=new Bird();
    bird[i].setIndex(i);
    bird[i].setPerceptionRadius(BIRD_PERCEPTION_RADIUS);
    bird[i].setBoundaries(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE);
    bird[i].setBoundBehaviour(BIRD_BOUND_BEHAVE_CLIP);
    bird[i].setPos(rndPlusMinVec(BIRD_INIT_POS_BOX_SIDE));
    bird[i].setMov(rndDirVec(BIRD_MAX_VELOCITY));
    bird[i].setMaxVel(BIRD_MAX_VELOCITY);
  }
}

void initSpacialData() {
  spacialData.init(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,BIRD_PERCEPTION_RADIUS);
}

// UPDATE FUNCTIONS:

void updateBirdsInPossibleRange() {
  for (int i=0;i<bird.length;i++) {
    spacialData.setBirdsInRange(bird[i]);
  }
}

void updatePosFromFlock() {
  for (int i=0;i<bird.length;i++) {
    bird[i].updatePosFromFlock();
  }
}


void posUpdateBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i].updatePos();
  }
}

void updateSpacialData() {
  spacialData.clear();
  for (int i=0;i<bird.length;i++) {
    spacialData.addBird(bird[i]);
  }
}

void drawBirds() {
  for (int i=0;i<bird.length;i++) {
    drawBirdSphere(bird[i],BIRD_SIZE,BIRD_COLOR);
    if (BIRD_PERCEPTION_RADIUS_DRAW) {
      drawBirdPerception(bird[i],BIRD_PERCEPTION_RADIUS,BIRD_LOCAL_COLOR);
    }
    drawMovVector(bird[i],BIRD_SIZE,BIRD_MOV_COLOR,BIRD_MOV_SHOW_EXAG);
  }
}





/*******************************
 * 
 * boring drawing functions
 * 
 ********************************/

void drawBirdSphere(Bird b, double rad, color c) {
  noStroke(); 
  fill(c);
  pushMatrix();
  vecTranslate(b.pos);
  sphereDetail(4);
  sphere((float)rad);
  popMatrix();
}

void drawBirdPerception(Bird b, double rad , color c) {
  stroke(red(c),blue(c),green(c),32);
  noFill(); 
  pushMatrix();
  vecTranslate(b.pos);
  //sphereDetail(5);
  //sphere((float)rad);
  ellipseMode(CENTER);
  ellipse(0,0,(float)rad,(float)rad);
  popMatrix();
}

void drawMovVector(Bird b, double rad, color c, double exag) {
  // draw mov vector onto surface of sphere with exaggerated length
  stroke(red(c),green(c),blue(c),64); 
  beginShape(LINES);
  // line startpoint:
  Vec p=new Vec(b.pos);
  Vec r=new Vec(vecMul(b.mov.getNormalized(),rad));
  p.add(r);
  vecVertex(b.pos);
  // line endpoint
  p.add(vecMul(b.mov,exag));
  vecVertex(p);
  endShape();
}

void drawBoundaries() {
  if (BOUNDARIES_DRAW) {
    stroke(BOUNDARIES_COLOR);
    noFill();
    pushMatrix();
    box((float)BOUNDARIES_X_RANGE,(float)BOUNDARIES_Y_RANGE,(float)BOUNDARIES_Z_RANGE);
    popMatrix();
  }
}

void showBirdsInRange() {
  for (int i=0;i<bird.length;i++) {
    float b=random(0,128);
    stroke(b,b,b,64);
    noFill();
    for (int t=0;t<bird[i].neighbourBirds.size();t++) {
      vecLine(bird[i].pos,((Bird)bird[i].neighbourBirds.elementAt(t)).pos);
    }
  }
}
