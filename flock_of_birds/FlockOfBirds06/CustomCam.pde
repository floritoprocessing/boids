int CAM_MODE_BIRD=0;
int CAM_MODE_OUTSIDE=1;
int CAM_MODE_CAGE=2;

class CustomCam {
  double camRd=0, camHei=0; // cam positioning for cage cam
  double xrange=1, yrange=1, zrange=1;
  BirdVecCam birdCam=new BirdVecCam(); // bird Cam
  Vec[] camVec=new Vec[3]; // camera vectors (eye,center,upaxis) for both cameras
  
  int camMode=1;
  
  // perspective settings:
  float fov;
  float aspect;
  float zNear;
  float zFar;
  

  CustomCam(double xr, double yr, double zr,float wid, float hei) {
    xrange=xr;
    yrange=yr;
    zrange=zr;
    fov=PI/2.3; //PI/3.0;
    aspect=wid/hei;
    float cameraZ = ((hei/2.0) / tan(PI*60.0/360.0));
    zNear=cameraZ/10.0;
    zFar=cameraZ*10.0;
    println("cameraZ: "+cameraZ);
  }
  
  void reset() {
    camRd=0;
    camHei=0;
  }
  
  void setCamMode(int cm) {
    camMode=cm;
  }
  
  void setCameraWithKeys() {
    if (keyPressed) {
      if (key=='1') {
        reset();
        setCamMode(0);
      }
      if (key=='2') {
        reset();
        setCamMode(1);
      }
      if (key=='3') {
        reset();
        setCamMode(2);
      }
    }
  }
  
  void update(boolean listenToMouse) {
    if (camMode==CAM_MODE_OUTSIDE||camMode==CAM_MODE_CAGE) {
      // CAMERA MOUNTED AROUND THE CAGE
      // MOUSE CAN MOVE THE CAMERA
      
      if (mousePressed&&listenToMouse) {      
        if (camMode==CAM_MODE_CAGE) {
          camRd-=2*Math.PI*(mouseX-pmouseX)/(float)width;
          camHei+=2*zrange*(mouseY-pmouseY)/(float)height;
        } else {
          camRd+=2*Math.PI*(mouseX-pmouseX)/(float)width;
          camHei-=2*zrange*(mouseY-pmouseY)/(float)height;
        }
        while (camRd<0) {camRd+=2*Math.PI;}
      }
    
      camVec[0]=new Vec(xrange*Math.cos(camRd),camHei,zrange*Math.sin(camRd));
      camVec[1]=new Vec(0,0,0);
      camVec[2]=new Vec(0,1,0);
      
      if (camMode==CAM_MODE_CAGE) {
        vecSwap(camVec[0],camVec[1]);
        camVec[1].z*=-1;
      }
      
    } else if (camMode==CAM_MODE_BIRD) {
      // CAMERA MOUNTED ON BIRD 0
      // NO INTERACTION POSSIBLE
      birdCam.update(bird[0]);
      camVec=birdCam.getCamVec();
    }
    vecCamera(camVec[0],camVec[1],camVec[2]);
    
//    float cameraZ = ((height/2.0) / tan(PI*60.0/360.0));
//    perspective(PI/2.2,width/(float)height, cameraZ/10.0, cameraZ*10.0);
//    perspective(PI/3.0, width/height, cameraZ/10.0, cameraZ*10.0) where cameraZ is ((height/2.0) / tan(PI*60.0/360.0));  
//    Syntax   perspective()
    //perspective(fov, aspect, zNear, zFar);
  }
  
  
}
