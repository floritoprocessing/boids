/* Separation:
 For each nearby character, 
 a repulsive force is computed by subtracting the positions of our character and the nearby character, 
 normalizing, and then applying a 1/r weighting. (works ok)
 These repulsive forces for each nearby character are summed together 
 to produce the overall steering force
 */

class SeparationVec extends Vec {
  SeparationVec() {
  }

  void update(Vector neighbours, Vec pos) {
    Vec totalAwayVec=new Vec();
    if (neighbours.size()>0) {
      for (int i=0;i<neighbours.size();i++) {
        Vec awayVec=new Vec(pos);
        awayVec.sub(((Bird)neighbours.elementAt(i)).pos);
        
        double r=awayVec.len();
        awayVec.div(r);  // normalize
        
        awayVec.div(r);  // apply 1/r weighting;
        totalAwayVec.add(awayVec);
      }
      setVec(totalAwayVec);
    }
    
  }
}
