/***************************************
 * 
 * FlockOfBirds05
 *    
 * Click and Drag to rotate the cage
 * The Birds in this example show the following behaviours:
 * Wander, Separation, Cohesion, Align
 * If Birds near the boundaries, they will turn
 * If Birds crash into boundaries they will enter at the other side
 * 
 ***************************************/

import java.util.Vector;

// see http://www.red3d.com/cwr/boids/
// also check http://www.red3d.com/cwr/steer/gdc99/

/* ---------------------------------------
 /  BIRD BEHAVIOR AND DRAWING SETTINGS
 /  --------------------------------------- */
 
double  BIRD_MAX_VELOCITY=15;         // maximum velocity
double  BIRD_MAX_FORCE=2;             // maximum steering force
double  BIRD_PERCEPTION_RADIUS=250;   // local perception radius
double  BIRD_INIT_POS_BOX_SIDE=600;   // initial random x/y/z coordinate box [-side,side]
boolean BIRD_SHOW_CONNECTIONS=false;  // show bird's neighbours (within perception radius)
double  BIRD_WALL_AVOID_DISTANCE=150;  // (horizontal) distance to a wall when bird starts turning

double  BIRD_STEER_WANDER_FAC=1.0;     // strength of wander
double  BIRD_STEER_SEPARATION_FAC=1.0; // strength of alignment
double  BIRD_STEER_COHESION_FAC=1.0;   // strength of cohesion
double  BIRD_STEER_ALIGN_FAC=1.0;      // strength of alignment

/* ---------------------------------------
 /  UNIVERSE SETTINGS
 /  --------------------------------------- */
 
double  BOUNDARIES_X_RANGE=1600;      // min/max x pos
double  BOUNDARIES_Y_RANGE=1600;      // min/max y pos
double  BOUNDARIES_Z_RANGE=1600;      // min/max z pos
color   BOUNDARIES_COLOR=#ddddff;     // color of bounding box
boolean BOUNDARIES_DRAW=true;         // show bounding box

// display variables
color  SPACIAL_DATA_COLOR=#88FF88;         // color of spacial bounding box


Bird[] bird=new Bird[80];
Bread[] bread=new Bread[100];
SpacialData spacialData=new SpacialData();
CustomCam myCamera;//=new CustomCam(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,800,600);

Vector allSliders=new Vector();
Slider sliderWander=new Slider(40,600-100-40,10,100,0.0,1.5,BIRD_STEER_WANDER_FAC,"Wander");
Slider sliderSeparation=new Slider(60,600-100-40,10,100,0.0,1.5,BIRD_STEER_SEPARATION_FAC,"Separation");
Slider sliderCohesion=new Slider(80,600-100-40,10,100,0.0,1.5,BIRD_STEER_COHESION_FAC,"Cohesion");
Slider sliderAlign=new Slider(100,600-100-40,10,100,0.0,1.5,BIRD_STEER_ALIGN_FAC,"Align");

PImage ground;



/* ---------------------------------------
 /  INITIALIZE
 /  --------------------------------------- */



void setup() {
  size(800,600,P3D);
  myCamera=new CustomCam(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,width,height);
  initBirds();
  initBread();
  initSpacialData();
  initTextures();
  allSliders.addElement(sliderWander);
  allSliders.addElement(sliderSeparation);
  allSliders.addElement(sliderCohesion);
  allSliders.addElement(sliderAlign);
}



/* ---------------------------------------
 /  MOUSE pressed and released
 /  --------------------------------------- */



void mousePressed() {
  for (int i=0;i<allSliders.size();i++) {
    Slider temp=(Slider)allSliders.elementAt(i);
    temp.startDrag();
  }
}
void mouseReleased() {
  for (int i=0;i<allSliders.size();i++) {
    Slider temp=(Slider)allSliders.elementAt(i);
    temp.stopDrag();
  }
}



/* ---------------------------------------
 /  DRAWing loop
 /  --------------------------------------- */



void draw() {
  // clear background:
  background(240,240,255);
//  lights();

  // set camera position
  myCamera.setCameraWithKeys();

  // update camera pos, don't update if dragging a slider
  myCamera.update(isNotOverSliderKnob()); 

  // update position of all Birds
  updateBirds();                

  // update SpacialData (which Bird is in which quadrant)
  updateSpacialData();

  // draw total universe boundaries
  if (BOUNDARIES_DRAW) {
    drawBoundaries();
  }

  // draw all Birds
  drawBirds();

  // calculate neighbouring Birds for all Birds
  updateBirdsInPossibleRange();

  // draw connections between Birds and neighbouring Birds before moving them
  if (BIRD_SHOW_CONNECTIONS) {
    showBirdNeighbourConnections();
  }

  // draw Bread
  //drawBread();

  // sliders:
  drawSlidersAndUpdateValues(); 
}



/* ---------------------------------------
 /  INITIALIZE FUNCTIONS
 /  --------------------------------------- */



void initBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i]=new Bird();
    bird[i].setIndex(i);
    bird[i].setWanderFac(BIRD_STEER_WANDER_FAC);
    bird[i].setSeparationFac(BIRD_STEER_SEPARATION_FAC);
    bird[i].setCohesionFac(BIRD_STEER_COHESION_FAC);//setCohesionFacs()
    bird[i].setAlignFac(BIRD_STEER_ALIGN_FAC);
    bird[i].setPerceptionRadius(BIRD_PERCEPTION_RADIUS);
    bird[i].setBoundaries(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE);
    bird[i].setBoundBehaviour(BIRD_BOUND_BEHAVE_CLIP);
    bird[i].setPos(rndPlusMinVec(BIRD_INIT_POS_BOX_SIDE));
    bird[i].setMov(rndPlusMinVec(BIRD_MAX_VELOCITY));
    bird[i].setWanderMax(2*BIRD_MAX_VELOCITY);
    bird[i].setMaxVel(BIRD_MAX_VELOCITY);
    bird[i].setMaxForce(BIRD_MAX_FORCE);
    bird[i].setWallAvoidDistance(BIRD_WALL_AVOID_DISTANCE);
    bird[i].Wander();
    //bird[i].setHunger(random(1));

    float b=80*random(1);
    if (b<40) {
      bird[i].setColor(color(0,215+b*random(1),0));
    } 
    else {
      bird[i].setColor(color(b-40,255,b-40));
    }

  }
}

void setWanderFacs(double f) {
  for (int i=0;i<bird.length;i++) {
    bird[i].setWanderFac(f);
  }
}
void setSeparationFacs(double f) {
  for (int i=0;i<bird.length;i++) {
    bird[i].setSeparationFac(f);
  }
}
void setCohesionFacs(double f) {
  for (int i=0;i<bird.length;i++) {
    bird[i].setCohesionFac(f);
  }
}
void setAlignFacs(double f) {
  for (int i=0;i<bird.length;i++) {
    bird[i].setAlignFac(f);
  }
}


void initBread() {
  for (int i=0;i<bread.length;i++) {
    bread[i]=new Bread(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE);
  }
}

void initSpacialData() {
  spacialData.init(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,BIRD_PERCEPTION_RADIUS);
}

void initTextures() {
  ground=loadImage("grass1.jpg");
}



/* ---------------------------------------
 /  UPDATE FUNCTIONS
 /  --------------------------------------- */
 
 

void updateBirdsInPossibleRange() {
  for (int i=0;i<bird.length;i++) {
    spacialData.setBirdsInRange(bird[i]);
  }
}

void updateBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i].update();
  }
}

void drawBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i].drawBody();
  }
}

void updateSpacialData() {
  spacialData.clear();
  for (int i=0;i<bird.length;i++) {
    spacialData.addBird(bird[i]);
  }
}

void showBirdNeighbourConnections() {
  for (int i=0;i<bird.length;i++) {
    bird[i].showBirdNeighbourConnections();
  }
}

boolean isNotOverSliderKnob() {
  boolean listenToMouse=true;
  for (int i=0;i<allSliders.size();i++) {
    Slider temp=(Slider)allSliders.elementAt(i);
    if (temp.dragging) {
      listenToMouse=false;
    }
  }
  return listenToMouse;
}



/* ---------------------------------------
 /  DRAWING/DISPLAYING FUNCTIONS
 /  --------------------------------------- */



// draw universe boundaries in BOUNDARIES_COLOR
void drawBoundaries() {
  stroke(BOUNDARIES_COLOR);
  noFill();
  pushMatrix();
  box((float)BOUNDARIES_X_RANGE,(float)BOUNDARIES_Y_RANGE,(float)BOUNDARIES_Z_RANGE);
  popMatrix();

  float x=(float)(BOUNDARIES_X_RANGE/2.0);
  float y=(float)(BOUNDARIES_Y_RANGE/2.0);
  float z=(float)(BOUNDARIES_Z_RANGE/2.0);


  pushMatrix();
  translate(0,y,0);
  rotateX(PI/2.0);
  fill(0,0,0);
  noStroke(); 
  textureMode(NORMAL); 
  drawFace(-x,-y,2*x,2*y,8);
  popMatrix();

}


void drawFace(float left, float top, float wid, float hei, float div) {
  // draws a flat surface by drawing subsurfaces to avoid visual distortion
  // --> try div=1 and see the distortion!
  for (int xf=0;xf<div;xf++) {
    for (int yf=0;yf<div;yf++) {
      beginShape(QUADS); 
      texture(ground); 
      float x1=left+xf*wid/div;
      ;
      float x2=left+(xf+1)*wid/div;
      float y1=top+yf*hei/div;
      float y2=top+(yf+1)*hei/div;

      float u1=xf/div;
      float u2=(xf+1)/div;
      float v1=yf/div;
      float v2=(yf+1)/div;

      vertex(x1,y1,u1,v1); 
      vertex(x2,y1,u2,v1); 
      vertex(x2,y2,u2,v2); 
      vertex(x1,y2,u1,v2); 
      endShape();
    }
  }
}


// draw all Breadcrumbs if they exist
void drawBread() {
  noStroke();
  fill(#CC6600);
  sphereDetail(4);
  for (int i=0;i<bread.length;i++) {
    if (bread[i].there) {
      pushMatrix();
      vecTranslate(bread[i].pos);
      sphere(3);
      popMatrix();
    }
  }
}


// DRAW SLIDERS
void drawSlidersAndUpdateValues() {
  camera();
  for (int i=0;i<allSliders.size();i++) {
    Slider temp=(Slider)allSliders.elementAt(i);
    temp.update();
    if (temp.dragging) {
      switch (i) {
      case 0: 
        setWanderFacs(temp.value); 
        break;
      case 1: 
        setSeparationFacs(temp.value); 
        break;
      case 2: 
        setCohesionFacs(temp.value); 
        break;
      case 3: 
        setAlignFacs(temp.value); 
        break;
      default: 
        break;
      }
    }
  }
}
