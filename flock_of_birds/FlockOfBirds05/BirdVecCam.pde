class BirdVecCam {
  Vec lMov=new Vec();
  Vec[] camVec=new Vec[3];
  
  BirdVecCam() {
    camVec[0]=new Vec();
    camVec[1]=new Vec();
    camVec[2]=new Vec(0,1,0);
  }
  
  void update(Bird b) {
    Vec tPos=vecAdd(vecAdd(b.pos,new Vec(0,-20,0)),vecMul(lMov,-3.0));
    lMov=vecDiv(vecAdd(vecMul(lMov,8.0),vecMul(b.mov,1.0)),9.0);
    //vecCamera(tPos,vecAdd(tPos,lMov),new Vec(0,1,0));
    camVec[0].setVec(tPos);
    camVec[1].setVec(vecAdd(tPos,lMov));
  }
  
  Vec[] getCamVec() {
    return camVec;
  }
}
