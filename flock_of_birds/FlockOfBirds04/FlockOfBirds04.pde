/***************************************
 * 
 * FlockOfBirds04
 *    
 * Click and Drag to rotate the cage
 * The Birds in this example show the following behaviours:
 * Wander, Separation, Cohesion, Align
 * 
 ***************************************/

import java.util.Vector;

// see http://www.red3d.com/cwr/boids/
// also check http://www.red3d.com/cwr/steer/gdc99/

// bird settings:
double  BIRD_MAX_VELOCITY=15;         // maximum velocity
double  BIRD_MAX_FORCE=2;             // maximum steering force
double  BIRD_PERCEPTION_RADIUS=250;   // local perception radius
double  BIRD_INIT_POS_BOX_SIDE=600;   // initial random x/y/z coordinate box [-side,side]
boolean BIRD_SHOW_CONNECTIONS=false;  // show bird's neighbours (within perception radius)
double  BIRD_WALL_AVOID_DISTANCE=150;  // (horizontal) distance to a wall when bird starts turning

double  BIRD_STEER_WANDER_FAC=1.0;     // strength of wander
double  BIRD_STEER_SEPARATION_FAC=1.0; // strength of alignment
double  BIRD_STEER_COHESION_FAC=1.0;   // strength of cohesion
double  BIRD_STEER_ALIGN_FAC=1.0;      // strength of alignment

// universe boundaries settings:
double  BOUNDARIES_X_RANGE=1600;      // min/max x pos
double  BOUNDARIES_Y_RANGE=1600;      // min/max y pos
double  BOUNDARIES_Z_RANGE=1600;      // min/max z pos
color   BOUNDARIES_COLOR=#52b0f9;     // color of bounding box
boolean BOUNDARIES_DRAW=true;         // show bounding box

// display variables
color  SPACIAL_DATA_COLOR=#88FF88;         // color of spacial bounding box

int CAM_MODE=1; // CAM_MODE_BIRD=0, CAM_MODE_OUTSIDE=1, CAM_MODE_CAGE=2;

Bird[] bird=new Bird[80];
Bread[] bread=new Bread[100];
SpacialData spacialData=new SpacialData();
CustomCam myCamera;//=new CustomCam(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,800,600);

void setup() {
  size(800,600,P3D);
  myCamera=new CustomCam(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,width,height);
  initBirds();
  initBread();
  initSpacialData();
}

void draw() {
  background(0xc1e1f9);

  // set camera position
  if (keyPressed) {
    if (key=='1') {CAM_MODE=0;myCamera.reset();}
    if (key=='2') {CAM_MODE=1;myCamera.reset();}
    if (key=='3') {CAM_MODE=2;myCamera.reset();}
  }
  myCamera.updateAndSet(CAM_MODE);
  
  // update position of all Birds
  updateBirds();                
  
  // update SpacialData (which Bird is in which quadrant)
  updateSpacialData();

  // draw total universe boundaries
  if (BOUNDARIES_DRAW) {drawBoundaries();}
  
  // draw all Birds
  drawBirds();
  
  // calculate neighbouring Birds for all Birds
  updateBirdsInPossibleRange();
  
  // draw connections between Birds and neighbouring Birds before moving them
  if (BIRD_SHOW_CONNECTIONS) {showBirdNeighbourConnections();}
  
  // draw Bread
  drawBread();
}

// INIT FUNCTIONS:

void initBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i]=new Bird();
    bird[i].setIndex(i);
    bird[i].setWanderFac(BIRD_STEER_WANDER_FAC);
    bird[i].setSeparationFac(BIRD_STEER_SEPARATION_FAC);
    bird[i].setCohesionFac(BIRD_STEER_COHESION_FAC);
    bird[i].setAlignFac(BIRD_STEER_ALIGN_FAC);
    bird[i].setPerceptionRadius(BIRD_PERCEPTION_RADIUS);
    bird[i].setBoundaries(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE);
    bird[i].setBoundBehaviour(BIRD_BOUND_BEHAVE_CLIP);
    bird[i].setPos(rndPlusMinVec(BIRD_INIT_POS_BOX_SIDE));
    bird[i].setMov(rndPlusMinVec(BIRD_MAX_VELOCITY));
    bird[i].setWanderMax(2*BIRD_MAX_VELOCITY);
    bird[i].setMaxVel(BIRD_MAX_VELOCITY);
    bird[i].setMaxForce(BIRD_MAX_FORCE);
    bird[i].setWallAvoidDistance(BIRD_WALL_AVOID_DISTANCE);
    
    //bird[i].setHunger(random(1));
    
    bird[i].Wander();

    float b=80*random(1);
    if (b<40) {
      bird[i].setColor(color(0,215+b*random(1),0));
    } 
    else {
      bird[i].setColor(color(b-40,255,b-40));
    }
    bird[i].setColor(color(137,87,23));

  }
}

void initBread() {
  for (int i=0;i<bread.length;i++) {
    bread[i]=new Bread(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE);
  }
}

void initSpacialData() {
  spacialData.init(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,BIRD_PERCEPTION_RADIUS);
}

// UPDATE FUNCTIONS:

void updateBirdsInPossibleRange() {
  for (int i=0;i<bird.length;i++) {
    spacialData.setBirdsInRange(bird[i]);
  }
}

void updateBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i].update();
  }
}

void updateSpacialData() {
  spacialData.clear();
  for (int i=0;i<bird.length;i++) {
    spacialData.addBird(bird[i]);
  }
}







/*******************************
 * 
 * boring drawing functions
 * 
 ********************************/

void drawBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i].drawBody();
  }
}

void showBirdNeighbourConnections() {
  for (int i=0;i<bird.length;i++) {
    bird[i].showBirdNeighbourConnections();
  }
}



// draw universe boundaries in BOUNDARIES_COLOR
void drawBoundaries() {
  stroke(BOUNDARIES_COLOR);
  noFill();
  pushMatrix();
  box((float)BOUNDARIES_X_RANGE,(float)BOUNDARIES_Y_RANGE,(float)BOUNDARIES_Z_RANGE);
  popMatrix();
}



// draw all Breadcrumbs if they exist
void drawBread() {
  noStroke();
  fill(#CC6600);
  sphereDetail(4);
  for (int i=0;i<bread.length;i++) {
    if (bread[i].there) {
      pushMatrix();
      vecTranslate(bread[i].pos);
      sphere(3);
      popMatrix();
    }
  }
}
