/***************************************
 * 
 * FlockOfBirds05_07
 *    
 * Click and Drag to rotate the cage
 * The Birds in this example show the following behaviours:
 * Wander, Separation, Cohesion, Align
 * 
 ***************************************/

import java.util.Vector;

// see http://www.red3d.com/cwr/boids/
// also check http://www.red3d.com/cwr/steer/gdc99/

// bird settings:
double  BIRD_MAX_VELOCITY=6;         // maximum velocity
double  BIRD_MAX_FORCE=1;             // maximum steering force
double  BIRD_PERCEPTION_RADIUS=250;   // local perception radius
double  BIRD_INIT_POS_BOX_SIDE=600;   // initial random x/y/z coordinate box [-side,side]
boolean BIRD_SHOW_CONNECTIONS=false;  // show bird's neighbours (within perception radius)
double  BIRD_WALL_AVOID_DISTANCE=100;  // (horizontal) distance to a wall when bird starts turning

double  BIRD_STEER_WANDER_FAC=1.0;     // strength of wander
double  BIRD_STEER_SEPARATION_FAC=1.0; // strength of alignment
double  BIRD_STEER_COHESION_FAC=1.0;   // strength of cohesion
double  BIRD_STEER_ALIGN_FAC=1.0;      // strength of alignment

// universe boundaries settings:
double  BOUNDARIES_X_RANGE=1600;      // min/max x pos
double  BOUNDARIES_Y_RANGE=1000;      // min/max y pos
double  BOUNDARIES_Z_RANGE=1600;      // min/max z pos
color   BOUNDARIES_COLOR=#ddddff;     // color of bounding box
boolean BOUNDARIES_DRAW=false;         // show bounding box

// display variables
color  SPACIAL_DATA_COLOR=#88FF88;         // color of spacial bounding box

Bird[] bird=new Bird[100];
SpacialData spacialData=new SpacialData();
CustomCam myCamera;//=new CustomCam(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,800,600);

Vector allSliders=new Vector();
Slider sliderWander=new Slider(40,40,10,100,0.0,1.5,BIRD_STEER_WANDER_FAC,"Wander");
Slider sliderSeparation=new Slider(60,40,10,100,0.0,1.5,BIRD_STEER_SEPARATION_FAC,"Separation");
Slider sliderCohesion=new Slider(80,40,10,100,0.0,1.5,BIRD_STEER_COHESION_FAC,"Cohesion");
Slider sliderAlign=new Slider(100,40,10,100,0.0,1.5,BIRD_STEER_ALIGN_FAC,"Align");

Ground ground=new Ground();

PImage panorama;

void setup() {
  size(800,600,P3D);
  myCamera=new CustomCam(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,width,height,3);
  initBirds();
  initSpacialData();
  allSliders.addElement(sliderWander);
  allSliders.addElement(sliderSeparation);
  allSliders.addElement(sliderCohesion);
  allSliders.addElement(sliderAlign);
  ground.init(3*BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,3*BOUNDARIES_Z_RANGE,20,20,15.0);
  panorama=loadImage("panorama3.jpg");
}

void mousePressed() {
  for (int i=0;i<allSliders.size();i++) {
    Slider temp=(Slider)allSliders.elementAt(i);
    temp.startDrag();
  }
}
void mouseReleased() {
  for (int i=0;i<allSliders.size();i++) {
    Slider temp=(Slider)allSliders.elementAt(i);
    temp.stopDrag();
  }
}

void draw() {
  background(240,240,255); 
  
  // listen to keypressed
  myCamera.input();
  
  // update camera pos, tell camera if user is over one of the sliders
  myCamera.update(sliderInactive());
  
  // update position of all Birds
  updateBirds();                
  
  // update SpacialData (which Bird is in which quadrant)
  updateSpacialData();

  // draw total universe boundaries
  if (BOUNDARIES_DRAW) {drawBoundaries();}
  
  // draw ground
  ground.toScreen();
  
  float step=36;
  //image(panorama,0,0);
  for (float rd=0;rd<2*PI;rd+=2*PI/step) {
    float x1=50*(float)BOUNDARIES_X_RANGE*sin(rd);
    float z1=50*(float)BOUNDARIES_Z_RANGE*cos(rd);
    
    float x2=50*(float)BOUNDARIES_X_RANGE*sin(rd+2*PI/step);
    float z2=50*(float)BOUNDARIES_Z_RANGE*cos(rd+2*PI/step);
    
    float u1=rd/(2*PI);
    float u2=(rd+2*PI/step)/(2*PI);
    
    noStroke();
    fill(0,0,0);
    textureMode(NORMAL);
    
    float yMin=-200*(float)BOUNDARIES_Y_RANGE/2.0;
    float yMax=200*(float)BOUNDARIES_Y_RANGE/2.0;
    float yR=200*(float)BOUNDARIES_Y_RANGE;
    
    for (float y=yMin;y<yMax/4.0;y+=yR/10.0) {
      float y2=y+yR/10.0;
      beginShape();
      texture(panorama);
      vertex(x1,y,z1,u1,(y-yMin)/yR);
      vertex(x1,y2,z1,u1,(y2-yMin)/yR);
      vertex(x2,y2,z2,u2,(y2-yMin)/yR);
      vertex(x2,y,z2,u2,(y-yMin)/yR);
      endShape();
    }
  }
  
  // draw all Birds
  drawBirds();
  
  // calculate neighbouring Birds for all Birds
  updateBirdsInPossibleRange();
  
  // draw connections between Birds and neighbouring Birds before moving them
  if (BIRD_SHOW_CONNECTIONS) {showBirdNeighbourConnections();}
  
  // sliders:
  sliderInteraction();
}

// INIT FUNCTIONS:

void initBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i]=new Bird();
    bird[i].setIndex(i);
    bird[i].setWanderFac(BIRD_STEER_WANDER_FAC);
    bird[i].setSeparationFac(BIRD_STEER_SEPARATION_FAC);
    bird[i].setCohesionFac(BIRD_STEER_COHESION_FAC);//setCohesionFacs()
    bird[i].setAlignFac(BIRD_STEER_ALIGN_FAC);
    bird[i].setPerceptionRadius(BIRD_PERCEPTION_RADIUS);
    bird[i].setBoundaries(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE);
    bird[i].setBoundBehaviour(BIRD_BOUND_BEHAVE_CLIP);
    bird[i].setPos(rndPlusMinVec(BIRD_INIT_POS_BOX_SIDE));
    bird[i].setMov(rndPlusMinVec(BIRD_MAX_VELOCITY));
    bird[i].setWanderMax(2*BIRD_MAX_VELOCITY);
    bird[i].setMaxVel(BIRD_MAX_VELOCITY);
    bird[i].setMaxForce(BIRD_MAX_FORCE);
    bird[i].setWallAvoidDistance(BIRD_WALL_AVOID_DISTANCE);
    
    //bird[i].setHunger(random(1));
    
    bird[i].Wander();

    float f=random(110,150);
    bird[i].setColor(color(f+random(-10,10),f+random(-10,10),f+random(-10,10)));
    
    float b=80*random(1);  
/*    if (b<40) {
    
      bird[i].setColor(color(0,215+b*random(1),0));
    } 
    else {
      bird[i].setColor(color(b-40,255,b-40));
    }
*/
  }
}

void setWanderFacs(double f) {
  for (int i=0;i<bird.length;i++) {
    bird[i].setWanderFac(f);
  }
}
void setSeparationFacs(double f) {
  for (int i=0;i<bird.length;i++) {
    bird[i].setSeparationFac(f);
  }
}
void setCohesionFacs(double f) {
  for (int i=0;i<bird.length;i++) {
    bird[i].setCohesionFac(f);
  }
}
void setAlignFacs(double f) {
  for (int i=0;i<bird.length;i++) {
    bird[i].setAlignFac(f);
  }
}



void initSpacialData() {
  spacialData.init(BOUNDARIES_X_RANGE,BOUNDARIES_Y_RANGE,BOUNDARIES_Z_RANGE,BIRD_PERCEPTION_RADIUS);
}

// UPDATE FUNCTIONS:

void updateBirdsInPossibleRange() {
  for (int i=0;i<bird.length;i++) {
    spacialData.setBirdsInRange(bird[i]);
  }
}

void updateBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i].update();
  }
}

void updateSpacialData() {
  spacialData.clear();
  for (int i=0;i<bird.length;i++) {
    spacialData.addBird(bird[i]);
  }
}

boolean sliderInactive() {
  boolean listenToMouse=true;
  for (int i=0;i<allSliders.size();i++) {
    Slider temp=(Slider)allSliders.elementAt(i);
    if (temp.dragging) {listenToMouse=false;}
  }
  return listenToMouse;
}

void sliderInteraction() {
  camera();
  for (int i=0;i<allSliders.size();i++) {
    Slider temp=(Slider)allSliders.elementAt(i);
    temp.update();
    if (temp.dragging) {
      switch (i) {
        case 0: setWanderFacs(temp.value); break;
        case 1: setSeparationFacs(temp.value); break;
        case 2: setCohesionFacs(temp.value); break;
        case 3: setAlignFacs(temp.value); break;
        default: break;
      }
    }
  }
}


/*******************************
 * 
 * boring drawing functions
 * 
 ********************************/

void drawBirds() {
  for (int i=0;i<bird.length;i++) {
    bird[i].drawBody();
  }
}

void showBirdNeighbourConnections() {
  for (int i=0;i<bird.length;i++) {
    bird[i].showBirdNeighbourConnections();
  }
}



// draw universe boundaries in BOUNDARIES_COLOR
void drawBoundaries() {
  stroke(BOUNDARIES_COLOR);
  noFill();
  pushMatrix();
  box((float)BOUNDARIES_X_RANGE,(float)BOUNDARIES_Y_RANGE,(float)BOUNDARIES_Z_RANGE);
  popMatrix();
}
