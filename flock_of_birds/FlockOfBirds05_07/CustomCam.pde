int CAM_MODE_BIRD=0;
int CAM_MODE_OUTSIDE=1;
int CAM_MODE_CAGE=2;
int CAM_MODE_DOOM=3;

class CustomCam {
  double camRd=0, camHei=0; // cam positioning for cage cam
  double xrange=1, yrange=1, zrange=1;
  BirdVecCam birdCam=new BirdVecCam(); // bird Cam
  DoomVecCam doomCam; // doom cam
  Vec[] camVec=new Vec[3]; // camera vectors (eye,center,upaxis) for both cameras
  
  int camMode=1;
  
  // perspective settings:
  float fov;
  float aspect;
  float zNear;
  float zFar;
  

  CustomCam(double xr, double yr, double zr,float wid, float hei, int _camMode) {
    xrange=xr;
    yrange=yr;
    zrange=zr;
    fov=PI/2.3; //PI/3.0;
    aspect=wid/hei;
    float cameraZ = ((hei/2.0) / tan(PI*60.0/360.0));
    zNear=cameraZ/10.0;
    zFar=cameraZ*10.0;
    //println("cameraZ: "+cameraZ);
    camMode=_camMode;
    doomCam=new DoomVecCam(xr,yr,zr);
  }
  
  void reset() {
    camRd=0;
    camHei=0;
  }
  
  void setCamMode(int cm) {
    camMode=cm;
  }
  
  void input() {
    if (keyPressed) {
      if (key=='1') {reset();setCamMode(CAM_MODE_OUTSIDE);}
      if (key=='2') {reset();setCamMode(CAM_MODE_CAGE);}
      if (key=='3') {reset();setCamMode(CAM_MODE_BIRD);}
      if (key=='4') {reset();setCamMode(CAM_MODE_DOOM);}
    }
  }
  
  void update(boolean listenToMouse) {
  
    // CAMERA OUTSIDE THE CAGE
    if (camMode==CAM_MODE_OUTSIDE) {
      if (mousePressed&&listenToMouse) {      
        camRd+=2*Math.PI*(mouseX-pmouseX)/(float)width;
        camHei-=2*zrange*(mouseY-pmouseY)/(float)height;
        while (camRd<0) {camRd+=2*Math.PI;}
      }
      camVec[0]=new Vec(xrange*Math.cos(camRd),camHei,zrange*Math.sin(camRd));
      camVec[1]=new Vec(0,0,0);
      camVec[2]=new Vec(0,1,0);
      
      vecCamera(camVec[0],camVec[1],camVec[2]);
    }
    
    // CAMERA IN THE CENTER OF THE CAGE
    if (camMode==CAM_MODE_CAGE) {
      if (mousePressed&&listenToMouse) {      
        camRd-=2*Math.PI*(mouseX-pmouseX)/(float)width;
        camHei+=2*zrange*(mouseY-pmouseY)/(float)height;
        while (camRd<0) {camRd+=2*Math.PI;}
      }
    
      camVec[0]=new Vec(0,0,0);
      camVec[1]=new Vec(xrange*Math.cos(camRd),camHei,-zrange*Math.sin(camRd));
      camVec[2]=new Vec(0,1,0);
      
      vecCamera(camVec[0],camVec[1],camVec[2]);
    }
    
    // CAMERA ON BIRD 0
    if (camMode==CAM_MODE_BIRD) {
      birdCam.update(bird[0]);
    }
    
    // CAMERA ON PERSON ON FLOOR
    if (camMode==CAM_MODE_DOOM) {
      doomCam.update();
    }
    
    
  }
  
  
}
