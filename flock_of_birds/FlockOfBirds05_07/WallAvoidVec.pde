class WallAvoidVec extends Vec {
  double x1=-1, x2=1; 
  double y1=-1, y2=1; 
  double z1=-1, z2=1; 
  double wallDist=0.1;
  
  Vec pos=new Vec();
  Vec mov=new Vec();
  
  WallAvoidVec() {
  }
  
  void setWalls(double _x1, double _x2, double _y1, double _y2, double _z1, double _z2) {
    x1=_x1;
    x2=_x2;
    y1=_y1;
    y2=_y2;
    z1=_z1;
    z2=_z2;
  }
  
  void setDistanceToWalls(double d) {
    wallDist=d;
  }
  
  boolean hitFloor() {
    return (y2-pos.y<wallDist&&mov.y>0);
  }
  
  boolean hitCeiling() {
    //return (pos.y-y1<wallDist&&mov.y<0);
    return (pos.y-y1<wallDist);
  }
  
  boolean hitXWall() {
    //return ((x2-pos.x<wallDist&&mov.x>0)||(pos.x-x1<wallDist&&mov.x<0));
    return ((x2-pos.x<wallDist)||(pos.x-x1<wallDist));
  }
  
  boolean hitZWall() {
    return ((z2-pos.z<wallDist)||(pos.z-z1<wallDist));
    //return ((z2-pos.z<wallDist&&mov.z>0)||(pos.z-z1<wallDist&&mov.z<0));
  }
  
  void update(Vec _pos, Vec _mov) {
    pos.setVec(_pos);
    mov.setVec(_mov);
    if (hitFloor()||hitCeiling()||hitXWall()||hitZWall()) {
      setVec(vecMul(pos,-1));
    } else {
      setVec(new Vec(0,0,0));
    }
  }
}
