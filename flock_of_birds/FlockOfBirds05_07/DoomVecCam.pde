class DoomVecCam {
  double xr=200,yr=200,zr=200;
  double speed=5;
  boolean walking=false;
  
  Vec[] cVec=new Vec[3];
  Vec orientation=new Vec();
  Vec floorOrientation=new Vec();
  double xRot=0, yRot=0-PI/4.0;
  
  DoomVecCam(double _xr, double _yr, double _zr) {
    xr=_xr; yr=_yr; zr=_zr;
    cVec[0]=new Vec(-xr/2.2,yr/2.0-100,-zr/2.2);
    cVec[1]=new Vec(0,0,1);
    cVec[2]=new Vec(0,1,0);
  }
  
  void update() {
    walking=false;
    if (mousePressed) {
      xRot-=0.05*(mouseY-height/2.0)/(float)height;
      yRot+=0.05*(mouseX-width/2.0)/(float)width;
    }
    
    if (xRot<0) {xRot=0;}
    if (xRot>PI/8.5) {xRot=PI/8.5;}
    
    orientation.setVec(0,0,1);
    orientation.rotX(xRot);
    orientation.rotY(yRot);
    
    floorOrientation.setVec(0,0,speed);
    floorOrientation.rotY(yRot);
    
    if (keyPressed) {
      if (key=='w'||key=='W') {
        cVec[0].add(floorOrientation);
        walking=true;
      }
      if (key=='s'||key=='S') {
        cVec[0].sub(floorOrientation);
        walking=true;
      }
      if (key=='d'||key=='D') {
        cVec[0].add(vecRotY(floorOrientation,PI/2.0));
        walking=true;
      }
      if (key=='a'||key=='A') {
        cVec[0].sub(vecRotY(floorOrientation,PI/2.0));
        walking=true;
      }
    }
    
    if (cVec[0].x<-xr/2.0) {cVec[0].x=-xr/2.0;}
    if (cVec[0].x>xr/2.0) {cVec[0].x=xr/2.0;}
    if (cVec[0].z<-zr/2.0) {cVec[0].z=-zr/2.0;}
    if (cVec[0].z>zr/2.0) {cVec[0].z=zr/2.0;}

    Vec walkPos=new Vec(cVec[0]);
    if (walking) {
      float freq=1.5*PI*millis()/1000.0;
      float amp=6;
      float f=abs(amp*sin(freq));
      if (f<3) {f=3;}
      walkPos.y-=f;
    }
    
    cVec[1]=vecAdd(walkPos,orientation);
    
    vecCamera(walkPos,cVec[1],cVec[2]);
  }
}
