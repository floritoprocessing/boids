class Slider {
  Vec pos=new Vec();
  Vec dim=new Vec();
  Vec sldPos=new Vec();
  Vec newSldPos=new Vec();
  Vec sldDim=new Vec();
  
  double rangeStart=0.0;
  double rangeEnd=2.0;
  double range=2.0;
  double value=0.0;
  
  String name="";
  
  double perc=0.0;
  int pressY=0;
  int offY=0;
  boolean dragging=false;

  boolean over=false;
  boolean overHandle=false;
  
  //PFont myFont;//=loadFont("arial.vlw");
  
  Slider(double px, double py, double w, double h, double r1, double r2, double initvalue, String s) {
    name=s;
    //
    //myFont=loadFont("C:/Processing/FlockOfBirds05/data/Verdana-48.vlw");
    //textFont(myFont,32);
   // text("hoi",10,10);
    
    pos.setVec(px,py,0);
    dim.setVec(w,h,0);
    sldDim.setVec(dim.x/2.0,4,0);

    rangeStart=r1;
    rangeEnd=r2;
    range=rangeEnd-rangeStart;
    value=initvalue;
    if (value<r1) {value=r1;}
    if (value>r2) {value=r2;}
    print(value);
    
    sldPos.setVec(vecSub(vecAdd(pos,dim),new Vec(w/2.0,0,0)));
    sldPos.y = pos.y+dim.y-sldDim.y-(dim.y-2*sldDim.y)*(value-rangeStart)/range;
    newSldPos.setVec(sldPos);
  }  



  void update() {
    // Check if mouse is over slider (to highlite slider):
    if (mouseX>pos.x&&mouseX<pos.x+dim.x&&mouseY>pos.y&&mouseY<pos.y+dim.y) {
      if (!over) {println("Slider: "+name+" ("+(int)(Math.round(value*100))/100.0+")");}
      over=true;
    } else {
      over=false;
    }
    if (over) {
      stroke(0,0,0,128);
    } 
    else {
      stroke(0,0,0,32);
    }
    fill(0,0,0,16);
    rectMode(CORNER);
    vecRect(pos,dim);

  
    // Check if mouse is over handle
    overHandle=(mouseX>sldPos.x-sldDim.x&&mouseX<sldPos.x+sldDim.x&&mouseY>sldPos.y-sldDim.y&&mouseY<sldPos.y+sldDim.y);
    rectMode(CENTER);
    if (overHandle) {
      stroke(255,0,0,128);
      fill(255,0,0,128);
    } 
    else {
      stroke(0,0,0,128);
      fill(0,0,0,128);
    }

    
    // check for dragging and set slider position
    if (dragging) {
      offY=mouseY-pressY;
      newSldPos=new Vec(vecAdd(sldPos,new Vec(0,offY,0)));
      if (newSldPos.y<pos.y+sldDim.y) {
        newSldPos.y=pos.y+sldDim.y;
      }
      if (newSldPos.y>pos.y+dim.y-sldDim.y) {
        newSldPos.y=pos.y+dim.y-sldDim.y;
      }
      vecRect(newSldPos,sldDim);
    } else {
      vecRect(sldPos,sldDim);
    }

    // calculate slider value depending on slider position
    perc=(pos.y+dim.y-sldDim.y-newSldPos.y)/(dim.y-2*sldDim.y);
    value=rangeStart+perc*range;
    if (dragging) {
      println("val: "+(int)(Math.round(value*100))/100.0);
    }
    
    // display slider value
    // camera
  }  


  void startDrag() {
    if (overHandle) {
      pressY=mouseY;
      dragging=true;
    }
  }


  void stopDrag() {
    if (dragging) {
      dragging=false;
      sldPos.setVec(newSldPos);
    }
  }
  
}
