/*
Alignment steering behavior gives an character the ability to align itself with 
 (that is, head in the same direction and/or speed as) other nearby characters, 
 as shown in Figure 16. Steering for alignment can be computed by finding all 
 characters in the local neighborhood (as described above for separation), 
 averaging together the velocity (or alternately, the unit forward vector) of the nearby characters. 
 This average is the �desired velocity,� and so the steering vector is the difference 
 between the average and our character�s current velocity (or alternately, its unit forward vector). 
 This steering will tend to turn our character so it is aligned with its neighbors. 
 */

class AlignVec extends Vec {
  AlignVec() {
  }
  void update(Vector neighbours, Vec mov) {
    if (neighbours.size()>0) {
      Vec averageMov=new Vec();
      for (int i=0;i<neighbours.size();i++) {
        averageMov.add(((Bird)neighbours.elementAt(i)).mov);
      }
      averageMov.div((double)neighbours.size());
      setVec(vecSub(averageMov,mov));
    } else {
      setVec(new Vec(0,0,0));
    }
  }
}

