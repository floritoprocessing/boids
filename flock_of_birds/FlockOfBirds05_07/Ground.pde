class Ground {
  double xr=200, zr=200;
  double xs=20, zs=20;
  int devX=10, devZ=10;
  Vec[][] points;
  color[][] cols;
  
  Ground() {
  }
  
  void init(double _xr, double _yr, double _zr, int _devX, int _devZ, double ym) {
    xr=_xr;
    zr=_zr;
    devX=_devX;
    devZ=_devZ;
    xs=xr/(float)devX;
    zs=zr/(float)devZ;
    points=new Vec[devX+1][devZ+1];
    cols=new color[devX][devZ];
    
    for (int x=0;x<=devX;x++) {
      for (int z=0;z<=devZ;z++) {
        points[x][z]=new Vec((float)-xr/2.0+x*xr/(float)devX,(float)(_yr/2.0-ym*Math.random()),(float)(-zr/2.0 + z*zr/(float)devZ));
      }
    }
    
    for (int x=0;x<devX;x++) {
      for (int z=0;z<devZ;z++) {
        cols[x][z]=color(0,180+random(30),0);
      }
    }
    
  }
  
  void toScreen() {
    noStroke();
    //fill(0,255,0);
    pushMatrix();
    beginShape(QUADS);
    for (int x=0;x<devX;x++) {
      for (int z=0;z<devZ;z++) {
        fill(cols[x][z]);
        vecVertex(points[x][z]);
        vecVertex(points[x+1][z]);
        vecVertex(points[x+1][z+1]);
        vecVertex(points[x][z+1]);
//        vertex((float)(-xr/2.0 + x*xr/(float)devX), (float)y, (float)(-zr/2.0 + z*zr/(float)devZ));
//        vertex((float)(-xr/2.0 + (x+1)*xr/(float)devX), (float)y, (float)(-zr/2.0 + z*zr/(float)devZ));
//        vertex((float)(-xr/2.0 + (x+1)*xr/(float)devX), (float)y, (float)(-zr/2.0 + (z+1)*zr/(float)devZ));
//        vertex((float)(-xr/2.0 + x*xr/(float)devX), (float)y, (float)(-zr/2.0 + (z+1)*zr/(float)devZ));
      }
    }
    endShape();
    popMatrix();
  }
}
