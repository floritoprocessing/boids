class Polygon {
  
  color strokeColor=color(0,0,0);
  color fillColor=color(255,255,255);
  
  Vector vertexes;
  
  Polygon() {
    vertexes=new Vector();
  }
  
  Polygon(Polygon p) {
    strokeColor=p.strokeColor;
    fillColor=p.fillColor;
    vertexes=new Vector(p.vertexes);
  }
  
  void addVertex(Vec v) {
    vertexes.add(new Vec(v));
  }
  
  void clearVertexes() {
    vertexes.clear();
  }
  
  void setFill(color c) {
    fillColor=c;
  }
  
  void rescale(float fac) {
    for (int i=0;i<vertexes.size();i++) {
      Vec v=(Vec)(vertexes.elementAt(i));
      v.mul(fac);
    }
  }
  
  void toScreen() {
    stroke(strokeColor);
    fill(fillColor);
    beginShape(POLYGON);
    for (int i=0;i<vertexes.size();i++) {
      vecVertex((Vec)(vertexes.elementAt(i)));
    }
    endShape();
  }
  
}
