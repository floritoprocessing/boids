class Bird {
  
  float speed=3.0;
  float fieldOfView=radians(120);
  
  float oldDirFac=100;         // move in old direction
  float randomDirFac=10;//50;       // move randomly
  float horizontalDirFac=1;    // move horizontally
  float separationDirFac=0.0;  
  float cohesionDirFac=0.5;    // move to average center of neighbourhood
  float alignmentDirFac=0.0;
  
  float bodyScale=4.0;
  color leftWingColor=color(20,180,20);
  color rightWingColor=color(10,210,0);
    
  Vec pos;
  Vec mov;
  Vec randomDir=new Vec();
  Vec horizontalDir=new Vec();
  Vec separationDir=new Vec();
  Vec cohesionDir=new Vec();
  Vec alignmentDir=new Vec();
  Vec newMov=new Vec();
  
  float bodyRotLon, bodyRotLat;
  
  Vector neighbours;
  int nrOfNeighbours=0;
  
  Shape body;
  
  Bird(Space s) {
    // SETUP POSITION AND MOVEMENT
    pos=s.randomPosition();
    mov=rndDirVec(speed);
    
    // CREATE BODY
    Polygon p=new Polygon();
    body=new Shape();
    
    Vec v1=new Vec(-2,1,-2);
    Vec v2=new Vec(0,1,2);
    Vec v3=new Vec(2,1,-2);
    Vec v4=new Vec(0,-1,-1.5);
    v1.rotY(PI/2.0);
    v2.rotY(PI/2.0);
    v3.rotY(PI/2.0);
    v4.rotY(PI/2.0);
    
    p=new Polygon();
    p.addVertex(v1);
    p.addVertex(v2);
    p.addVertex(v4);
    p.setFill(leftWingColor);
    body.addPolygon(p);
    
    p=new Polygon();
    p.addVertex(v2);
    p.addVertex(v3);
    p.addVertex(v4);
    p.setFill(rightWingColor);
    body.addPolygon(p);
    
    body.rescale(bodyScale);
    
    s.setBirdPosition(this);
  }
  
  
  /*
  Vector getNeighbours() {
    Vector out=new Vector();
    return out;
  }
  */
  
  
  void newMoveVector(Space s) {
    
    // FIND BIRDS IN AREA:
    neighbours=new Vector();
    neighbours.addAll(s.getNeighbours(this));
    nrOfNeighbours=neighbours.size();
    
    // Random: steer to a new random direction based on current movement (Wander)
    randomDir.setVec(mov);
    randomDir.rotX(random(-0.3,0.3));
    randomDir.rotY(random(-0.3,0.3));
    randomDir.rotZ(random(-0.3,0.3));
    
    // Horizontal: steer to be flying horizontal
    horizontalDir.setVec(mov);
    horizontalDir.y=0;
    
    // Separation: steer to avoid crowding local flockmates 
    separationDir.setVec(0,0,0);
    
    // Cohesion: steer to move toward the average position of local flockmates 
    Vec avPos=new Vec();
    for (int i=0;i<nrOfNeighbours;i++) {
      Bird b=(Bird)(neighbours.elementAt(i));
      avPos=vecAdd(avPos,b.getPosition());
    }
    if (nrOfNeighbours>0) {
      avPos.div(float(neighbours.size()));
      cohesionDir.setVec(vecSub(avPos,pos));
    } else {
      cohesionDir.setVec(0,0,0);
    }
    
    // Alignment: steer towards the average heading of local flockmates 
    alignmentDir.setVec(0,0,0);
    
    newMov.setVec(0,0,0);
    newMov.add(vecMul(mov,oldDirFac));
    newMov.add(vecMul(randomDir,randomDirFac));
    newMov.add(vecMul(horizontalDir,horizontalDirFac));
    newMov.add(vecMul(separationDir,separationDirFac));
    newMov.add(vecMul(cohesionDir,cohesionDirFac));
    newMov.add(vecMul(alignmentDir,alignmentDirFac));
    newMov.setLen(speed);
    
    mov.setVec(newMov);
  }
  
  
  
  void neighbourToScreen() {
    Vec p1=new Vec(pos);
    p1.z=-p1.z;
    for (int j=0;j<neighbours.size();j++) {
      Bird b2=(Bird)(neighbours.elementAt(j));
      Vec p2=new Vec(b2.getPosition());
      p2.z=-p2.z;
      stroke(255,0,0,random(1)<0.5?64:255);
      vecLine(p1,p2);
    }
  }
  
  void calcOrientation() {
    double[] lonlat=mov.getLonLat();
    bodyRotLon=-(float)(Math.PI-lonlat[0]);
    bodyRotLat=(float)(-lonlat[1]);
  }
  
  
  
  void move(Space s) {
    pos.add(mov);
    s.clipInside(pos);
    s.setBirdPosition(this);
  }
  
  
  
  void toScreen() {
    pushMatrix();
    translate((float)pos.x,(float)pos.y,-(float)pos.z);
    rotateY(bodyRotLon);
    rotateZ(bodyRotLat);
    body.toScreen();
    popMatrix();
  }
  
  
  
  Vec getPosition() {
    return pos;
  }
  
  Vec getMovement() {
    return mov;
  }
  
  float getFieldOfView() {
    return fieldOfView;
  }
    
}
