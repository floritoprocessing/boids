class Cam {

  public static final int TYPE_FIXED_POSITION=0;
  public static final int TYPE_TRIPOD=1;
  public static final int TYPE_FOCUS_CENTER=2;
  public static final int TYPE_BIRDCAM=3;

  int type=0;

  Vec eye=new Vec(), cen=new Vec(), up=new Vec(0,1,0);
  Vec followPos=new Vec(), targetPos=new Vec();
  //double lon,lat,r;

  Cam(int _type) {
    type=_type;
  }

  Cam(int _type, Vec _eye, Vec _cen, Vec _up) {
    type=_type;
    eye.setVec(_eye);
    cen.setVec(_cen);
    up.setVec(_up);
  }

  void make(Bird b) {
    if (type==TYPE_TRIPOD) {
      Vec viewDir=vecSub(cen,eye);
      viewDir.rotX((mouseY-pmouseY)/float(height));
      viewDir.rotY((mouseX-pmouseX)/float(width));

      cen.setVec(vecAdd(eye,viewDir));
    } 
    else if (type==TYPE_FOCUS_CENTER) {

      Vec viewDir=vecSub(eye,cen);
      viewDir.rotY(0.2*(mouseX-width/2.0)/float(width));
      eye.setVec(vecAdd(cen,viewDir));

    } 
    else if (type==TYPE_BIRDCAM) {
      
      targetPos=vecDiv(vecAdd(vecMul(targetPos,4),vecMul(b.getPosition(),1)),5.0);
      
      followPos=vecDiv(vecAdd(vecMul(followPos,40),vecMul(targetPos,1)),41.0);
      
      eye.setVec(vecAdd(vecComponentMul(followPos,new Vec(1,1,-1)),new Vec(0,-50,0)));;
      cen.setVec(vecComponentMul(targetPos,new Vec(1,1,-1)));
      
    }

    vecCamera(eye,cen,up);
  }

}
