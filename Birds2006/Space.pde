class Space {
  
  float xdim, ydim, zdim;
  Vec dimensions;
  int xAmount, yAmount, zAmount;
  float subSize, subSizeSQ;
  
  Vector[][][] birdsInSpace;
  
  Space(float xd, float yd, float zd, float sSize) {
    xdim=xd;
    ydim=yd;
    zdim=zd;
    subSize=sSize;
    subSizeSQ=subSize*subSize;
    dimensions=new Vec(xdim,ydim,zdim);
    xAmount=int(xdim/subSize);
    yAmount=int(ydim/subSize);
    zAmount=int(zdim/subSize);
    birdsInSpace=new Vector[xAmount][yAmount][zAmount];
    for (int x=0;x<xAmount;x++) for (int y=0;y<yAmount;y++) for (int z=0;z<zAmount;z++) birdsInSpace[x][y][z]=new Vector();
  }
  
  Vec randomPosition() {
    return new Vec(random(xdim),random(ydim),random(zdim));
  }
  
  void clipInside(Vec v) {
    v.clip(new Vec(xdim,ydim,zdim));
  }
  
  Vec getCenter() {
    return new Vec(xdim/2.0,ydim/2.0,zdim/2.0);
  }
  
  int[] positionToIndex(Vec v) {
    int[] out=new int[3];
    Vec o=new Vec(Math.floor(v.x/xdim*xAmount),Math.floor(v.y/ydim*yAmount),Math.floor(v.z/zdim*zAmount));
    out[0]=(int)(o.x);
    out[1]=(int)(o.y);
    out[2]=(int)(o.z);
    return out;
  }
  
  void clearBirdPositions() {
    for (int x=0;x<xAmount;x++) for (int y=0;y<yAmount;y++) for (int z=0;z<zAmount;z++) birdsInSpace[x][y][z]=new Vector();
  }
  
  void setBirdPosition(Bird b) {
    int[] i=positionToIndex(b.getPosition());
    birdsInSpace[i[0]][i[1]][i[2]].add(b);
  }
  
  void toScreen() {
    stroke(0,0,0);
    noFill();
    for (int x=0;x<xAmount;x++) {
      for (int y=0;y<yAmount;y++) {
        for (int z=0;z<zAmount;z++) {
          point(x*subSize,y*subSize,-z*subSize);
        }
      }
    }
    
    noStroke();
    fill(128,128,128);
    beginShape(QUADS);
    vertex(0,ydim,0);
    vertex(xdim,ydim,0);
    vertex(xdim,ydim,-zdim);
    vertex(0,ydim,-zdim);
    endShape();
  }
  
  void showPositionOf(Bird b) {
    int[] i=positionToIndex(b.getPosition());
    noFill();
    stroke(255,0,0,32);
    pushMatrix();
    translate((i[0]+0.5)*subSize,(i[1]+0.5)*subSize,-(i[2]+0.5)*subSize);
    box(subSize,subSize,subSize);
    popMatrix();
  }
  
  
  // find all birds in neighbouring cubes 
  Vector getNeighbours(Bird b) {
    Vector out=new Vector();    
    for (float xo=-0.5;xo<=0.5;xo++) {
      for (float yo=-0.5;yo<=0.5;yo++) {
        for (float zo=-0.5;zo<=0.5;zo++) {
          Vec offset=new Vec(xo,yo,zo);
          offset.mul(subSize);
          int[] i=positionToIndex(vecAdd(b.getPosition(),offset));
          if (i[0]>=0&&i[0]<xAmount&&i[1]>=0&&i[1]<yAmount&&i[2]>=0&&i[2]<zAmount) {
            out.addAll(birdsInSpace[i[0]][i[1]][i[2]]);
          }
        }
      } 
    }
    // remove itself:
    out.remove(b);
    out.trimToSize();
    
    // create new vector with all birds within 'sighting range' from vector of birds in all cubes
    // checks also for distance
    Vector out2=new Vector();
    for (int ix=0;ix<out.size();ix++) {
      // calc angle between mov and other bird position
      Bird b2=(Bird)(out.elementAt(ix));
      Vec b2ToB=vecSub(b2.getPosition(),b.getPosition());
      double ang=vecAngleBetween(b.getMovement(),b2ToB);
      if (ang<b.getFieldOfView()) {
        double distB2toBSQ=b2ToB.lenSQ();
        if (distB2toBSQ<subSizeSQ) out2.add(b2);
      }
    }
    return out2;
  }
  
  /*
  void showNeighboursOf(Bird b) {
    Vector neighbours=getNeighbours(b);
    Vec p1=new Vec(b.getPosition());
    p1.z=-p1.z;
    for (int j=0;j<neighbours.size();j++) {
      Bird b2=(Bird)(neighbours.elementAt(j));
      Vec p2=new Vec(b2.getPosition());
      p2.z=-p2.z;
      stroke(255,0,0,random(1)<0.5?64:255);
      vecLine(p1,p2);
    }
  }
  */
  
}
