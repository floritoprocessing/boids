class Shape {
  
  Vector polygons;
  
  Shape() {
    polygons=new Vector();
  }
  
  void addPolygon(Polygon p) {
    polygons.add(new Polygon(p));
  }
  
  void rescale(float fac) {
    for (int i=0;i<polygons.size();i++) {
      Polygon p=(Polygon)(polygons.elementAt(i));
      p.rescale(fac);
    }
  }
  
  void toScreen() {
    for (int i=0;i<polygons.size();i++) {
      Polygon p=(Polygon)(polygons.elementAt(i));
      p.toScreen();
    }
  }
  
}
