// Birds2006

import java.util.Vector;

int BIRDS_AMOUNT=100;
int SPACE_WID=900;
int SPACE_HEI=300;
int SPACE_DEP=900;
int SPACE_SUBSIZE=100;


Vector birds;
Space space;
Cam[] cam;
int currentCam=3;

int frameNr=0, frameCountMax=250;
long frameTime=0, totalTime=0;

void setup() {
  size(800,480,P3D);
  
  cam=new Cam[4];
  Vec eye=new Vec(SPACE_WID/2.0,SPACE_HEI/2.0,0);
  Vec cen=new Vec(SPACE_WID/2.0,SPACE_HEI/2.0,-SPACE_DEP/2.0);
  Vec up=new Vec(0,1,0);
  
  cam[0]=new Cam(Cam.TYPE_FIXED_POSITION,eye,cen,up);
  cam[1]=new Cam(Cam.TYPE_TRIPOD,eye,cen,up);
  cam[2]=new Cam(Cam.TYPE_FOCUS_CENTER,eye,cen,up);
  cam[3]=new Cam(Cam.TYPE_BIRDCAM);
    
  birds=new Vector();
  space=new Space(SPACE_WID,SPACE_HEI,SPACE_DEP,SPACE_SUBSIZE);
  
  for (int i=0;i<BIRDS_AMOUNT;i++) birds.add(new Bird(space));
  
  // make first bird red:
  Bird b=(Bird)(birds.elementAt(0));
  Polygon p=(Polygon)(b.body.polygons.elementAt(0));
  p.fillColor=color(255,0,0);
  p=(Polygon)(b.body.polygons.elementAt(1));
  p.fillColor=color(255,0,0);
  
  println("press [1]-[4] to toggle cameraske");
}

void keyPressed() {
  if (key=='1') currentCam=0;
  if (key=='2') currentCam=1;
  if (key=='3') currentCam=2;
  if (key=='4') currentCam=3;
}

void draw() {
  frameTime=System.nanoTime();
  background(240,240,240);
  
  cam[currentCam].make((Bird)(birds.elementAt(0)));
  
  // create a new movement for birds depending on olds birds positions:
  for (int i=0;i<birds.size();i++) {
    Bird b=(Bird)(birds.elementAt(i));
    b.newMoveVector(space);
  }
  
  // clear all bird positions in space
  space.clearBirdPositions();
  
  for (int i=0;i<birds.size();i++) {
    Bird b=(Bird)(birds.elementAt(i));
    b.calcOrientation();
    b.move(space);
    b.toScreen();
  }  
    
  //space.showPositionOf(b);
  //space.showNeighboursOf((Bird)(birds.elementAt(0)));
  ((Bird)(birds.elementAt(0))).neighbourToScreen();
  //println((((Bird)(birds.elementAt(0))).getPosition()).toString());
  space.toScreen();
  
  totalTime+=(System.nanoTime()-frameTime);
  frameNr++;
  if (frameNr==frameCountMax) {
    println(frameCountMax+" frames of calculating and drawing birds: "+totalTime/1000000000.0+" seconds");
    println("-> that is "+(totalTime/1000000000.0)/float(frameCountMax)+" seconds per frame ("+float(frameCountMax)/(totalTime/1000000000.0)+"fps)");
    println("-> thus "+(totalTime/1000000.0)/float(frameCountMax*BIRDS_AMOUNT)+" milliseconds per bird per frame");
  }
}
