import processing.core.PApplet;
import processing.core.PMatrix3D;
import processing.core.PVector;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class Camera {

	private PApplet pa;
	private PVector eye = new PVector();
	private PVector center = new PVector();
	private PVector orientation = new PVector(0,1,0);
	private float distance;
	private float yrot;
	
	public Camera(PApplet pa, float distance) {
		this.pa = pa;
		this.distance = distance;
		makeEye();
	}
	
	/**
	 * creates eye vector from distance and yrot
	 */
	private void makeEye() {
		eye.x = (float)(distance*Math.sin(yrot));
		eye.y = 0;
		eye.z = (float)(distance*Math.cos(yrot));
	}

	public void make() {
		pa.camera(	eye.x,eye.y,eye.z,
					center.x,center.y,center.z,
					orientation.x,orientation.y,orientation.z);
	}

	/**
	 * @param mouseX
	 * @param mouseY
	 * @param pmouseX
	 * @param pmouseY
	 */
	public void mouseDragged(int mouseX, int mouseY, int pmouseX, int pmouseY) {
		yrot += (mouseX-pmouseX)*0.003f;
		makeEye();
	}
}
