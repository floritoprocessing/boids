import java.util.ArrayList;

import florito.boids.Bird;
import florito.boids.BirdWorld;
import florito.boids.Vec;
import processing.core.PApplet;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class FloidTester extends PApplet {

	private static final long serialVersionUID = -1227677471081837695L;

	public static void main(String[] args) {
		PApplet.main("FloidTester");
	}
	
	
	BirdWorld world;
	Camera cam;
	public void settings() {
		size(1024,576,P3D);
	}
	public void setup() {
		
		smooth();
		world = new BirdWorld(-640, 640, -360, 360, -640, 640, 160);
		world.createBirds(200);
		cam = new Camera(this,1200);
	}
	
	public void mouseDragged() {
		cam.mouseDragged(mouseX,mouseY,pmouseX,pmouseY);
	}
	
	public void draw() {
		background(255);
		
		cam.make();
		
		drawCage();
		drawBirds();
		
		world.update();
	}

	/**
	 * Draws all birds
	 */
	private void drawBirds() {
		int steps = 5;
		float exag = 2;
		ArrayList<Bird> birds = world.getBirds();
		boolean first = false;
		for (Bird b:birds) {
			
			Vec p0 = b.getPos();
			Vec step = Vec.div(b.getMov(), steps);
			step.mult(exag);
			Vec p1 = Vec.add(p0,step);
			
			/*
			 * bird
			 */
			for (int i=0;i<steps;i++) {
				strokeWeight(steps+1-i);
				if (first) {
					stroke(128,0,0);
				} else {
					stroke(0,128,0);
				}
				line(p0.x,p0.y,p0.z, p1.x,p1.y,p1.z);
				p0 = p1;
				p1 = Vec.add(p0,step);
			}
			
			/*
			 * range
			 */
			if (first) {
				pushMatrix();
				float r = world.getInfluenceRadius();
				translate(b.getPos());
				strokeWeight(1);
				stroke(128,0,0,32);
				sphereDetail(6);
				sphere(r);
				popMatrix();
			}
			
			/*
			 * Locals
			 */
			if (first) {
				strokeWeight(1);
				stroke(128,0,0);
				ArrayList<Bird> local = world.getLocalFlockmates(b);
				Vec one = b.getPos();
				for (Bird l:local) {
					Vec other = l.getPos();
					line(one,other);
				}
			}
			
			
			first = false;
		}
	}

	/**
	 * @param a
	 * @param b
	 */
	private void line(Vec a, Vec b) {
		line(a.x,a.y,a.z,b.x,b.y,b.z);
	}

	/**
	 * @param pos
	 */
	private void translate(Vec v) {
		translate(v.x, v.y, v.z);
	}

	/**
	 * Draws the cage boundaries
	 */
	private void drawCage() {
		float x0 = world.getxMin();
		float x1 = world.getxMax();
		float y0 = world.getyMin();
		float y1 = world.getyMax();
		float z0 = world.getzMin();
		float z1 = world.getzMax();
		noFill();
		stroke(0);
		strokeWeight(1);
		quad(x0, y0, z0, x1, y0, z0, x1, y1, z0, x0, y1, z0); // back
		quad(x0, y0, z1, x1, y0, z1, x1, y1, z1, x0, y1, z1); // front
		quad(x0, y0, z0, x0, y1, z0, x0, y1, z1, x0, y0, z1); // left
		quad(x1, y0, z0, x1, y1, z0, x1, y1, z1, x1, y0, z1); // right
		quad(x0, y0, z0, x0, y0, z1, x1, y0, z1, x1, y0, z0); // top
		quad(x0, y1, z0, x0, y1, z1, x1, y1, z1, x1, y1, z0); // top
	}
	
	private void quad(float x0, float y0, float z0,
			float x1, float y1, float z1,
			float x2, float y2, float z2,
			float x3, float y3, float z3) {
		beginShape(QUAD);
		vertex(x0,y0,z0);
		vertex(x1,y1,z1);
		vertex(x2,y2,z2);
		vertex(x3,y3,z3);
		endShape();
	}
}
