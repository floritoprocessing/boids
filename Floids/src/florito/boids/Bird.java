/**
 * 
 */
package florito.boids;

import java.util.ArrayList;

/**
 * @author Marcus
 * 
 */
public class Bird {

	private static float speed = 5;
	
	private static float wander = 		0.1f;//0.08f;
	private static float separation = 	0.1f;//0.02f;
	private static float alignment = 	0.1f;//0.05f;//0.05f;
	private static float cohesion = 	0.12f;

	BirdWorld world;
	Vec pos = new Vec();
	Vec mov = new Vec();
	Vec newMov = new Vec();

	public Bird(BirdWorld world, float x, float y, float z) {
		this.world = world;
		pos.set(x, y, z);
		mov = Vec.random(speed);

	}

	public Vec getPos() {
		return (Vec) pos.clone();
	}

	public Vec getMov() {
		return (Vec) mov.clone();
	}

	/**
	 * <LI>separation: steer to avoid crowding local flockmates 
	 * <LI>alignment: steer towards the average heading of local flockmates 
	 * <LI>cohesion: steer to movetoward the average position of local flockmates
	 */
	public void behave() {
		ArrayList<Bird> localBirds = world.getLocalFlockmates(this);
		ArrayList<Vec> localPos = new ArrayList<Vec>();
		ArrayList<Vec> allPos = new ArrayList<Vec>();
		ArrayList<Vec> localMov = new ArrayList<Vec>();
		for (Bird b:localBirds) {
			localPos.add(b.pos);
			allPos.add(b.pos);
			localMov.add(b.mov);
		}
		allPos.add(pos);
		
		
		/*
		 * Wander
		 */
		Vec wanderVector = Vec.random(speed);
		wanderVector.normalizeTo(speed*wander);
		
		/*
		 * Separation
		 */
		Vec averageAllPos = Vec.getAverage(allPos);
		Vec separationVector = new Vec();
		if (!averageAllPos.isNullVec()) {
			separationVector = Vec.sub(pos, averageAllPos);
			separationVector.normalizeTo(speed*separation);
		}
		
		/*
		 * Alignment
		 */
		Vec averageMov = Vec.getAverage(localMov);
		Vec alignmentVector = new Vec();
		if (!averageMov.isNullVec()) {
			alignmentVector = Vec.sub(averageMov,mov);
			alignmentVector.normalizeTo(speed*alignment);
		}
		
		/*
		 * Cohesion
		 */
		Vec averagePos = Vec.getAverage(localPos);
		Vec cohesionVector = new Vec();
		if (!averagePos.isNullVec()) {
			cohesionVector = Vec.sub(averagePos, pos);
			cohesionVector.normalizeTo(speed*cohesion);
		}
		
		newMov = Vec.add(mov,wanderVector,separationVector,alignmentVector,cohesionVector);//	alignmentVector));
		newMov.normalizeTo(speed);
	}
	
	public void updateNewMov() {
		mov.set(newMov);
	}

	/**
	 * Moves the bird and repeats world boundaries
	 * 
	 * @param world
	 */
	public void move() {
		pos.add(mov);
		/*
		 * Repeat world boundaries
		 */
		if (pos.x < world.xMin) {
			pos.x += world.xRange;
		} else if (pos.x >= world.xMax) {
			pos.x -= world.xRange;
		}
		if (pos.y < world.yMin) {
			pos.y += world.yRange;
		} else if (pos.y >= world.yMax) {
			pos.y -= world.yRange;
		}
		if (pos.z < world.zMin) {
			pos.z += world.zRange;
		} else if (pos.z >= world.zMax) {
			pos.z -= world.zRange;
		}
	}

}
