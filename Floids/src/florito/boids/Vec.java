/**
 * 
 */
package florito.boids;

import java.util.ArrayList;

/**
 * @author Marcus
 *
 */
public class Vec implements Cloneable {
	
	public static Vec add(Vec a, Vec b) {
		return new Vec(a.x+b.x,a.y+b.y,a.z+b.z);
	}
	
	public static Vec add(Vec a, Vec b, Vec c) {
		return new Vec(a.x+b.x+c.x,a.y+b.y+c.y,a.z+b.z+c.z);
	}
	
	public static Vec add(Vec... vecs) {
		Vec out = new Vec();
		for (Vec v:vecs) out.add(v);
		return out;
	}

	public static Vec div(Vec a, float f) {
		return new Vec(a.x/f, a.y/f, a.z/f);
	}
	
	public static Vec mult(Vec a, float f) {
		return new Vec(a.x*f, a.y*f, a.z*f);
	}
	
	public static Vec NULL_VECTOR = new Vec(0,0,0);

	/**
	 * Creates a random vector with length l
	 * @param l
	 * @return
	 */
	public static Vec random(float l) {
		Vec out = new Vec();
		out.x = (float)((Math.random()-0.5)*l);
		out.y = (float)((Math.random()-0.5)*l);
		out.z = (float)((Math.random()-0.5)*l);
		out.normalizeTo(l);
		return out;
	}
	
	public static Vec sub(Vec a, Vec b) {
		return new Vec(a.x-b.x,a.y-b.y,a.z-b.z);
	}
	
	public float x, y, z;
	
	public Vec() {
	}
	
	public Vec(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Vec(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec(Vec v) {
		x = v.x;
		y = v.y;
		z = v.z;
	}
	
	public void add(Vec v) {
		x += v.x;
		y += v.y;
		z += v.z;
	}
	
	@Override
	public Object clone() {
		return new Vec(x,y,z);
	}
	
	public void div(float f) {
		x /= f;
		y /= f;
		z /= f;
	}
	
	/**
	 * Returns true if the vector is the null vector
	 * @return
	 */
	public boolean isNullVec() {
		return x==0&&y==0&&z==0;
	}

	/**
	 * Returns the length (magnitude) of the vector
	 * @return
	 */
	public float length() {
		return (float)Math.sqrt(x*x+y*y+z*z);
	}

	public void mult(float f) {
		x *= f;
		y *= f;
		z *= f;
	}
	
	/**
	 * Normalises the vector
	 */
	public void normalize() {
		if (!isNullVec()) {
			float fac = 1/length();
			mult(fac);
		}
	}
	
	/**
	 * Normalises the vector to a give length
	 * @param l
	 */
	public void normalizeTo(float l) {
		if (!isNullVec()) {
			float len = length();
			float fac = l/len;
			mult(fac);
		}
	}

	public void set(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public void set(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void set(Vec v) {
		x = v.x;
		y = v.y;
		z = v.z;
	}

	public void sub(Vec v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;
	}

	@Override
	public String toString() {
		return "Vec("+x+","+y+","+z+")";
	}

	/**
	 * Shortens the vector if it's longer than maxLenght
	 * @param maxLength
	 */
	public void truncate(float maxLength) {
		if (!isNullVec()) {
			float length = length();
			if (length>maxLength) {
				mult(maxLength/length);
			}
		}
	}

	/**
	 * @param a
	 * @param b
	 * @return
	 */
	public static float distance(Vec a, Vec b) {
		return (Vec.sub(a, b)).length();
	}

	/**
	 * @param vecs
	 * @return
	 */
	public static Vec getAverage(ArrayList<Vec> vecs) {
		if (vecs.size()==0) {
			return NULL_VECTOR;
		}
		Vec average = new Vec();
		for (Vec v:vecs) {
			average.add(v);
		}
		average.div(vecs.size());
		return average;
	}
	
}
