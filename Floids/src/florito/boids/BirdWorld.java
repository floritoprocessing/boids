/**
 * 
 */
package florito.boids;

import java.util.ArrayList;

/**
 * @author Marcus
 *
 */
public class BirdWorld {
	
	float xMin, xMax, yMin, yMax, zMin, zMax;
	float xRange, yRange, zRange;
	float influenceRadius;
	
	ArrayList<Bird> birds = new ArrayList<Bird>();

	public BirdWorld(
			float xMin, float xMax, 
			float yMin, float yMax, 
			float zMin, float zMax,
			float influenceRadius) {
		
		this.xMin = Math.min(xMin,xMax);
		this.xMax = Math.max(xMin,xMax);
		this.yMin = Math.min(yMin,yMax);
		this.yMax = Math.max(yMin,yMax);
		this.zMin = Math.min(zMin,zMax);
		this.zMax = Math.max(zMin,zMax);
		xRange = this.xMax-this.xMin;
		yRange = this.yMax-this.yMin;
		zRange = this.zMax-this.zMin;
		this.influenceRadius = influenceRadius;
	}

	public float getInfluenceRadius() {
		return influenceRadius;
	}
	
	public float getxMin() {
		return xMin;
	}

	public float getxMax() {
		return xMax;
	}

	public float getyMin() {
		return yMin;
	}

	public float getyMax() {
		return yMax;
	}

	public float getzMin() {
		return zMin;
	}

	public float getzMax() {
		return zMax;
	}

	/**
	 * @param amount
	 */
	public void createBirds(int amount) {
		for (int i=0;i<amount;i++) {
			birds.add(new Bird(this,0.7f*random(xMin,xMax),0.7f*random(yMin,yMax),0.7f*random(zMin,zMax)));
		}
	}
	
	private static float random(float v0, float v1) {
		return (float)(v0+Math.random()*(v1-v0));
	}

	/**
	 * @return
	 */
	public ArrayList<Bird> getBirds() {
		return birds;
	}

	/**
	 * Move all birds
	 */
	public void update() {
		
		for (Bird b:birds) {
			b.behave();
		}
		for (Bird b:birds) {
			b.updateNewMov();
		}
		
		for (Bird b:birds) {
			b.move();
		}
	}

	/**
	 * @param bird
	 * @return
	 */
	public ArrayList<Bird> getLocalFlockmates(Bird bird) {
		/*
		 * crude method:
		 */
		ArrayList<Bird> local = new ArrayList<Bird>();
		for (Bird b:birds) {
			if (Vec.distance(bird.pos,b.pos)<influenceRadius) {
				if (!b.equals(bird)) {
					local.add(b);
				}
			}
		}
		return local;
	}
	
	
	
}
